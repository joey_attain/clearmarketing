<?php get_header(); ?>

<div class="page-banner" style="background-color: #373d43;">
    <div class="breadcrumbs-bar">
	    <div class="column row">
		    <?php
			    if ( function_exists('yoast_breadcrumb') ) {
			    yoast_breadcrumb('
			    <p id="breadcrumbs">','</p>
			    ');
		    }
	    ?>
	    </div>
    </div>
    <div class="column row">
        <h1 class="page-banner-heading"><?php _e( '404 - Page Not Found', 'bonestheme' ); ?></h1>
    </div>
</div>

	<div id="content">

		<div id="inner-content" class="column row">

			<main id="main" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

				<article id="post-not-found" class="hentry">

					<section class="entry-content">

						<p style="text-align: center;"><?php _e( 'The page you were looking for was not found, but maybe try looking again!', 'bonestheme' ); ?></p>
						<br>

					</section>

				</article>

			</main>

		</div>

	</div>

<?php get_footer(); ?>
