<?php get_header(); ?>

<div class="page-banner" style="background-image: url(/wp-content/uploads/join-us-banner.jpg);">
    <div class="breadcrumbs-bar">
	    <div class="column row">
		    <?php
			    if ( function_exists('yoast_breadcrumb') ) {
			    yoast_breadcrumb('
			    <p id="breadcrumbs">','</p>
			    ');
		    }
	    ?>
	    </div>
    </div>
    <div class="column row">
        <!-- <h1 class="page-banner-heading" makeBold="Join us">Be a part of something great! Join us</h1> -->
    </div>
</div>

	<div id="content">

		<div id="inner-content" class="row column">

			<main id="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

					<div class="thin-col" style="text-align: center;">
						<h2 style="color:#20ace0">Come and join our team!</h2>
						<p>We are now recruiting, we are looking for switched on individuals that enjoy a challenge and can bring some creativity to our team.</p>
					</div>
					<br><br>

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

						<?php
						$job_salary = get_post_meta( get_the_ID(), 'job_salary', true );
						$job_loc = get_post_meta( get_the_ID(), 'job_loc', true );
						$job_pos = get_post_meta( get_the_ID(), 'job_pos', true );
						$job_colorScheme = get_post_meta( get_the_ID(), 'job_colorScheme', true );
						$thumbnail = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
						?>

						<article id="post-<?php the_ID(); ?>" <?php post_class( 'job-card norm' ); ?> role="article">

							<div class="job-elem">
							    
							    <div class="lr image-col" style="background-image: url('<?=($thumbnail) ? $thumbnail : "" ?>'); background-color: <?php echo esc_html($job_colorScheme) ?>;"></div>

							    <div class="lr content-col">
							    	
								    <h3><?php the_title(); ?></h3>
								    <div class="job-details">
								    	<div>
								    		<i class="fa fa-gbp" aria-hidden="true" style="background-color:<?php echo esc_html($job_colorScheme); ?>;"></i><span class="sr-only">Salary:</span>
								    		<span><?php echo esc_html($job_salary); ?></span>
								    	</div>
								    	<div>
								    		<i class="fa fa-map-marker" aria-hidden="true" style="background-color:<?php echo esc_html($job_colorScheme); ?>;"></i><span class="sr-only">Location:</span>
								    		<span><?php echo esc_html($job_loc); ?></span>
								    	</div>
								    	<div>
								    		<i class="fa fa-clock-o" aria-hidden="true" style="background-color:<?php echo esc_html($job_colorScheme); ?>;"></i><span class="sr-only">Position:</span>
								    		<span><?php echo esc_html($job_pos); ?></span>
								    	</div>
								    </div>
								    <?php the_excerpt(); ?>
								    <a href="<?php the_permalink() ?>" class="btn-hollow-dynamic-alt" style="border-color:<?php echo esc_html($job_colorScheme); ?>;">View full job role</a>
								    <a href="/contact-us/" class="btn-dynamic"  style="background-color:<?php echo esc_html($job_colorScheme); ?>;border-color:<?php echo esc_html($job_colorScheme); ?>;">Apply for job</a>

							    </div>

							</div>

						</article>
					<?php endwhile; ?>

							<?php bones_page_navi(); ?>

					<?php else : ?>

							<article id="post-not-found" class="hentry">
								<header class="article-header">
									<h1><?php _e( 'Oops, Jobs Not Found!', 'bonestheme' ); ?></h1>
								</header>
							</article>

					<?php endif; ?>

					<div class="thin-col" style="text-align: center;">
						<h2 style="color:#20ace0">Send us your CV</h2>
						<p>Clear Marketing would love to hear how we worked with your business! Send us your thoughts about us and how we worked together.</p>
						<a href="/contact-us/" class="btn-hollow-primary">Contact us</a>
					</div>
					<br><br>

				</main>

		</div>

	</div>

<?php get_footer(); ?>
