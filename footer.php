<div class="page-bottom">
	<div class="column row">
		<a class="logo" href="<?php echo home_url(); ?>" rel="nofollow" title="Clear Marketing">

		<!-- Generator: Adobe Illustrator 21.0.2, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
		<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
			 viewBox="0 0 164 167" style="enable-background:new 0 0 164 167;" xml:space="preserve">
		<path d="M99.7,88.7c-0.6,0-1.3,0-1.9,0.1c-0.7,0.1-1.3,0.3-1.9,0.5c-0.6,0.3-1,0.7-1.4,1.1c-0.4,0.5-0.6,1.1-0.6,1.9
			c0,1.5,0.5,2.5,1.4,3c0.9,0.9,2.2,1.1,3.8,1.1c1,0,1.8,0,2.5-0.1c0.7,0,1.4-0.1,1.9-0.2v-6.8c-0.3-0.1-0.8-0.3-1.4-0.4
			C101.4,88.8,100.7,88.7,99.7,88.7z"/>
		<path d="M76.1,79.5c-0.9,0-1.8,0.2-2.5,0.5c-0.7,0.4-1.3,0.8-1.8,1.4s-0.9,1.2-1.2,2c-0.3,0.8-0.5,1.5-0.5,2.3h11.3
			c0-1.8-0.4-3.2-1.4-4.4C79.1,80.1,77.8,79.5,76.1,79.5z"/>
		<path d="M80.3,22c-35.1,0-63.6,28.5-63.6,63.6s28.5,63.6,63.6,63.6s63.6-28.5,63.6-63.6S115.4,22,80.3,22z M51,99
			c-0.4,0.1-0.9,0.2-1.5,0.3c-0.6,0.1-1.1,0.2-1.8,0.3c-0.8,0.2-1.4,0.3-1.9,0.3c-1.8,0-3.4-0.3-4.8-0.9c-1.4-0.6-2.5-1.4-3.5-2.4
			c-0.9-1-1.6-2.3-2.1-3.7s-0.7-3-0.7-4.8c0-1.7,0.3-3.3,0.8-4.8s1.2-2.7,2.2-3.8c0.9-1.1,2.1-1.9,3.4-2.5c1.4-0.6,2.9-0.9,4.5-0.9
			c1,0,2,0.1,3.1,0.3c1,0.2,2,0.5,2.9,0.8l-0.9,3.4c-0.6-0.3-1.3-0.5-2.1-0.7c-0.8-0.2-1.6-0.3-2.5-0.3c-2.3,0-4,0.7-5.2,2.1
			c-1.2,1.4-1.8,3.5-1.8,6.2c0,1.2,0.1,2.3,0.4,3.4c0.3,1,0.7,1.9,1.3,2.6c0.6,0.7,1.4,1.3,2.3,1.7c0.9,0.4,2.1,0.6,3.4,0.6
			c1.1,0,2-0.1,2.9-0.3c0.9-0.2,1.6-0.4,2-0.7l0.6,3.4C51.8,98.7,51.4,98.9,51,99z M63,99.8c-2.5-0.1-4.3-0.6-5.3-1.6
			s-1.6-2.6-1.6-4.8V66.2l4.1-0.7v27.2c0,0.7,0.1,1.2,0.2,1.7c0.1,0.4,0.3,0.8,0.6,1c0.3,0.3,0.6,0.5,1,0.6c0.4,0.1,1,0.2,1.6,0.3
			L63,99.8z M85.5,88.5c0,0.3,0,0.5,0,0.8H70c0.2,2.4,0.9,4.1,2,5.4c1.2,1.2,3.1,1.8,5.6,1.8c1.4,0,2.6-0.1,3.6-0.4
			c1-0.2,1.7-0.5,2.2-0.7l0.6,3.4c-0.5,0.3-1.4,0.5-2.6,0.8c-1.2,0.3-2.6,0.4-4.2,0.4c-2,0-3.7-0.3-5.1-0.9c-1.4-0.6-2.6-1.4-3.6-2.5
			c-0.9-1-1.6-2.3-2.1-3.7c-0.4-1.5-0.6-3.1-0.6-4.8c0-2,0.3-3.8,0.9-5.3s1.4-2.7,2.3-3.7c1-1,2.1-1.7,3.3-2.2
			c1.2-0.5,2.5-0.7,3.8-0.7c3.1,0,5.4,1,7,2.9c1.6,1.9,2.4,4.8,2.4,8.7C85.5,88,85.5,88.2,85.5,88.5z M107.5,99
			c-0.3,0.1-0.8,0.1-1.5,0.2c-0.6,0.1-1.3,0.2-2.1,0.3c-0.8,0.1-1.6,0.2-2.5,0.2c-0.9,0.1-1.8,0.1-2.7,0.1c-1.3,0-2.5-0.1-3.5-0.4
			c-1.1-0.3-2-0.7-2.8-1.2c-0.8-0.6-1.4-1.3-1.8-2.2c-0.4-0.9-0.7-2-0.7-3.4c0-1.2,0.3-2.3,0.8-3.2s1.2-1.6,2.1-2.2
			c0.9-0.6,1.9-1,3.1-1.2c1.2-0.3,2.4-0.4,3.7-0.4c0.4,0,0.8,0,1.3,0.1c0.4,0,0.8,0.1,1.2,0.2c0.4,0.1,0.7,0.1,1,0.2
			c0.3,0.1,0.5,0.1,0.6,0.1V85c0-0.7-0.1-1.3-0.2-2c-0.1-0.7-0.4-1.2-0.8-1.7c-0.4-0.5-0.9-0.9-1.5-1.2c-0.7-0.3-1.5-0.5-2.5-0.5
			c-1.3,0-2.5,0.1-3.5,0.3s-1.8,0.4-2.2,0.6l-0.5-3.4c0.5-0.2,1.4-0.5,2.6-0.7c0.9-0.2,2.2-0.3,3.7-0.3c1.6,0,3,0.2,4.1,0.6
			c1.1,0.4,2,1,2.7,1.8c0.7,0.8,1.2,1.7,1.5,2.7c0.3,1.1,0.4,2.2,0.4,3.5V99z M123.7,80c-0.7-0.2-1.7-0.2-2.9-0.2
			c-0.8,0-1.5,0.1-2.2,0.2c-0.7,0.2-1.2,0.3-1.5,0.3v19H113V77.7c1-0.3,2.1-0.7,3.6-1c1.6-0.3,3.2-0.5,4.9-0.5c0.3,0,0.7,0,1.2,0.1
			c0.5,0,0.9,0.1,1.3,0.2c0.4,0.1,0.8,0.1,1.2,0.2c0.4,0.1,0.6,0.1,0.8,0.2l-0.7,3.5C125,80.3,124.5,80.1,123.7,80z"/>
		<path d="M81.4,85.8v-0.1h0C81.4,85.7,81.4,85.8,81.4,85.8z"/>
		</svg>
		</a>
		<div class="contact-info">
			<div class="row">
				<div class="medium-6 column">
					<h3>Manchester</h3>
					<div class="contact-line"><i class="fa fa-phone" aria-hidden="true"></i><span class="sr-only">Phone Number:</span><?php echo get_theme_mod( 'bones_contact_tel' ); ?></div>
					<div class="contact-line"><i class="fa fa-envelope" aria-hidden="true"></i><span class="sr-only">Email Address:</span> <a href="mailto:gemma@clearmarketing.co.uk">gemma@clearmarketing.co.uk</a></div>
					<div class="contact-line"><i class="fa fa-map-marker" aria-hidden="true"></i><span class="sr-only">Our Location:</span> <?php echo do_shortcode('[address-manchester]')?></div>
				</div>
				<div class="medium-6 column">
					<h3>Glasgow</h3>
					<div class="contact-line"><i class="fa fa-phone" aria-hidden="true"></i><span class="sr-only">Phone Number:</span><?php echo get_theme_mod( 'bones_contact_tel_two' ); ?></div>
					<div class="contact-line"><i class="fa fa-envelope" aria-hidden="true"></i><span class="sr-only">Email Address:</span> <a href="mailto:matthew@clearmarketing.co.uk">matthew@clearmarketing.co.uk</a></div>
					<div class="contact-line"><i class="fa fa-map-marker" aria-hidden="true"></i><span class="sr-only">Our Location:</span> <?php echo do_shortcode('[address-glasgow]')?></div>
				</div>
			</div>
		</div>
	</div>
</div>

<footer class="footer" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">

	<div id="inner-footer" class="row column">

		<div class="top-row-left">
			<p class="source-org copyright">
				<?php if ( get_theme_mod( 'bones_copyright' ) ) {
					echo get_theme_mod( 'bones_copyright' );
				} ?>
				&nbsp;Website by <a href="http://www.attain.uk.com" target="_blank"><strong>ATTAIN</strong></a>
				&nbsp; View our <a href="/privacy-policy/">Privacy Policy</a>
			</p>
		</div>

		<div class="top-row-right">
			<?php echo do_shortcode('[social-links]'); ?>
		</div>

	</div>

</footer>

</div>

<?php // all js scripts are loaded in library/bones.php ?>
<?php wp_footer(); ?>

</body>

</html> <!-- end of site. what a ride! -->
