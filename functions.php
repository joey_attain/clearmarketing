<?php
/*
This is where you can drop your custom functions or
just edit things like thumbnail sizes, header images,
sidebars, comments, etc.
*/

// LOAD BONES CORE (if you remove this, the theme will break)
require_once( 'library/bones.php' );

//show_admin_bar(false);

$isCompressed = false;

//Making jQuery to load from Google Library
function replace_jquery() {
  if (!is_admin()) {
    // comment out the next two lines to load the local copy of jQuery
    wp_deregister_script('jquery');
    wp_register_script('jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js', false, '1.11.3');
    wp_enqueue_script('jquery');
  }
}
add_action('init', 'replace_jquery');

// CUSTOMIZE THE WORDPRESS ADMIN (off by default)
require_once( 'library/admin.php' );

/*********************
LAUNCH BONES
Let's get everything up and running.
*********************/

function bones_ahoy() {

  //Allow editor style.
  add_editor_style( get_stylesheet_directory_uri() . '/library/css/editor-style.css' );

  // let's get language support going, if you need it
  load_theme_textdomain( 'bonestheme', get_template_directory() . '/library/translation' );

  // USE THIS TEMPLATE TO CREATE CUSTOM POST TYPES EASILY
  //require_once( 'library/custom-post-type.php' );

  // launching operation cleanup
  add_action( 'init', 'bones_head_cleanup' );
  // A better title
  add_filter( 'wp_title', 'rw_title', 10, 3 );
  // remove WP version from RSS
  add_filter( 'the_generator', 'bones_rss_version' );
  // remove pesky injected css for recent comments widget
  add_filter( 'wp_head', 'bones_remove_wp_widget_recent_comments_style', 1 );
  // clean up comment styles in the head
  add_action( 'wp_head', 'bones_remove_recent_comments_style', 1 );
  // clean up gallery output in wp
  add_filter( 'gallery_style', 'bones_gallery_style' );

  // enqueue base scripts and styles
  add_action( 'wp_enqueue_scripts', 'bones_scripts_and_styles', 999 );
  // ie conditional wrapper

  // launching this stuff after theme setup
  bones_theme_support();

  // adding sidebars to Wordpress (these are created in functions.php)
  add_action( 'widgets_init', 'bones_register_sidebars' );

  // cleaning up random code around images
  add_filter( 'the_content', 'bones_filter_ptags_on_images' );
  // cleaning up excerpt
  add_filter( 'excerpt_more', 'bones_excerpt_more' );

} /* end bones ahoy */

// let's get this party started
add_action( 'after_setup_theme', 'bones_ahoy' );

/************* OEMBED SIZE OPTIONS *************/

if ( ! isset( $content_width ) ) {
	$content_width = 680;
}

/************* THUMBNAIL SIZE OPTIONS *************/

// Thumbnail sizes
add_image_size( 'bones-thumb-600', 600, 150, true );
add_image_size( 'bones-thumb-300', 300, 100, true );

/*
to add more sizes, simply copy a line from above
and change the dimensions & name. As long as you
upload a "featured image" as large as the biggest
set width or height, all the other sizes will be
auto-cropped.

To call a different size, simply change the text
inside the thumbnail function.

For example, to call the 300 x 100 sized image,
we would use the function:
<?php the_post_thumbnail( 'bones-thumb-300' ); ?>
for the 600 x 150 image:
<?php the_post_thumbnail( 'bones-thumb-600' ); ?>

You can change the names and dimensions to whatever
you like. Enjoy!
*/

add_filter( 'image_size_names_choose', 'bones_custom_image_sizes' );

function bones_custom_image_sizes( $sizes ) {
    return array_merge( $sizes, array(
        'bones-thumb-600' => __('600px by 150px'),
        'bones-thumb-300' => __('300px by 100px'),
    ) );
}

/*
The function above adds the ability to use the dropdown menu to select
the new images sizes you have just created from within the media manager
when you add media to your content blocks. If you add more image sizes,
duplicate one of the lines in the array and name it according to your
new image size.
*/

/************* THEME CUSTOMIZE *********************/
require_once( 'library/customizer.php' );

/************* GOOGLE CAPTCHA *************/
/*
  Retrieves Google Captcha code from Customizer
  (library/customizer.php);

  Use echo get_catpcha_siteKey(); to retrieve the site key and
  echo get_catpcha_secret(); to retrieve the secret key.
*/
function get_catpcha_siteKey() {
  if ( get_theme_mod( 'bones_google_captcha_siteKey' ) ) {
    return get_theme_mod( 'bones_google_captcha_siteKey' );
  }
}
function get_catpcha_secret() {
  if ( get_theme_mod( 'bones_google_captcha_secret' ) ) {
    return get_theme_mod( 'bones_google_captcha_secret' );
  }
}

/************* DISPLAY ELEMENTS *********************/
require_once( 'library/elems.php' );

//custom post types
require_once( get_template_directory().'/library/homebanner-post-type.php' );
require_once( get_template_directory().'/library/testimonial-post-type.php' );
require_once( get_template_directory().'/library/work-post-type.php' );
require_once( get_template_directory().'/library/job-post-type.php' );

//custom metaboxes
require_once( get_template_directory().'/library/cbm2.php' );

/************* VISUAL COMPOSER ELEMENTS *********************/
add_action( 'vc_before_init', 'vc_before_init_actions' );

function vc_before_init_actions() {

   require_once( 'vc-templates/cta-box.php' );
   require_once( 'vc-templates/cta-clickable-box.php' );
   require_once( 'vc-templates/cta-strip.php' );
   require_once( 'vc-templates/cta-icon-text.php' );
   require_once( 'vc-templates/cta-image.php' );
   require_once( 'vc-templates/cta-hover-text.php' );

}

/*
This is a modification of a function found in the
twentythirteen theme where we can declare some
external fonts. If you're using Google Fonts, you
can replace these fonts, change it in your scss files
and be up and running in seconds.
*/
function bones_fonts() {
  wp_enqueue_style('googleFonts', '//fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic');
}

add_action('wp_enqueue_scripts', 'bones_fonts');

// Issue with showing VC shortcodes in search results 
// Strip out Visual Composer specific shortcodes
add_filter('relevanssi_pre_excerpt_content', 'rlv_trim_vc_shortcodes');
function rlv_trim_vc_shortcodes($content) {
    $content = preg_replace('/\[\/?vc_.*?\]/', '', $content);
    return $content;
}

// fix wrong calculated post per page number
function my_post_count_queries( $query ) {
  if (!is_admin() && $query->is_main_query()){
    if(is_home()){
       $query->set('posts_per_page', 4);
    }
  }
}
add_action( 'pre_get_posts', 'my_post_count_queries' );

function set_posts_per_page_for_work_cpt( $query ) {
  if ( !is_admin() && $query->is_main_query() && is_post_type_archive( 'work' ) ) {
    $query->set( 'posts_per_page', '12' );
  }
}
add_action( 'pre_get_posts', 'set_posts_per_page_for_work_cpt' );

/* DON'T DELETE THIS CLOSING TAG */ ?>
