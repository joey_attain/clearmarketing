<?php

	$response = isset($_POST["captcha"]) ? $_POST["captcha"] : '';
	$secret = isset($_POST["secret"]) ? $_POST["secret"] : '';

	$verify = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret={$secret}&response={$response}");
	$captcha_success = json_decode($verify);

	if ($captcha_success->success==false) {
	    echo json_encode('captcha verification failure');
	    exit();
	} else {
		echo json_encode('success!');
		exit();
	}

?>