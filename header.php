<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

	<head>
		<meta charset="utf-8">

		<?php // force Internet Explorer to use the latest rendering engine available ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title><?php wp_title(''); ?></title>

		<?php // mobile meta (hooray!) ?>
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1"/>

		<?php // icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) ?>
		<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-touch-icon.png">
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->
		<?php // or, set /favicon.ico for IE10 win ?>
		<meta name="msapplication-TileColor" content="#f01d4f">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">
        <meta name="theme-color" content="#121212">

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<!-- Schema.org markup for Google+ -->
		<meta itemprop="name" content="Clear Marketing">
		<meta itemprop="description" content="We're a strategic creative agency who deliver solutions to engage with your customers">
		<meta itemprop="image" content="http://clearmarketing.co.uk/wp-content/uploads/clear-open-graph.jpg">

		<!-- Twitter Card data -->
		<meta name="twitter:card" content="summary_large_image">
		<meta name="twitter:site" content="http://clearmarketing.co.uk">
		<meta name="twitter:title" content="Clear Marketing">
		<meta name="twitter:description" content="We're a strategic creative agency who deliver solutions to engage with your customers">
		<meta name="twitter:creator" content="@theattainteam">
		<meta name="twitter:image:src" content="http://clearmarketing.co.uk/wp-content/uploads/clear-open-graph.jpg">

		<!-- Open Graph data -->
		<meta property="og:title" content="Welcome to Clear Marketing" />
		<meta property="og:type" content="website" />
		<meta property="og:url" content="http://clearmarketing.co.uk" />
		<meta property="og:image" content="http://clearmarketing.co.uk/wp-content/uploads/clear-open-graph.jpg" />
		<meta property="og:description" content="We're a strategic creative agency who deliver solutions to engage with your customers" />
		<meta property="og:site_name" content="Clear Marketing" />

		<script src="https://use.typekit.net/ubo8amd.js"></script>
		<script>try{Typekit.load({ async: true });}catch(e){}</script>

		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

		<?php // wordpress head functions ?>
		<?php wp_head(); ?>
		<?php // end of wordpress head ?>

		<?php // Google Analytics
		if ( get_theme_mod( 'bones_google_tracking' ) ) {
		  echo get_theme_mod( 'bones_google_tracking' );
		}
		// end analytics ?>

			

		<style>
        p.hover-top-text {
            left: 0;
        }

		</style>
		
		<script>
			var doc = document.documentElement;
doc.setAttribute('data-useragent', navigator.userAgent);
		</script>
		

<style>
.sticky-grid .wpb_wrapper, .space-grid .wpb_wrapper {
    display: flex;
    flex: 1;
}
	_::-webkit-full-page-media, _:future, :root .sticky-grid .wpb_wrapper, .space-grid .wpb_wrapper {
		height: auto;
}
	
	_::-webkit-full-page-media, _:future, :root .vc_section.vc_section-flex {
		display: block;
	}
	
	_::-webkit-full-page-media, _:future, :root .cta-image .on-hover {
		display: block;
	}
	
	_::-webkit-full-page-media, _:future, :root .cta-image .on-hover .hover-top-text {
		position: relative;
	}
	
.sticky-grid .wpb_wrapper > section, .space-grid .wpb_wrapper > section {
    display: flex;
    flex: 1;
	height: auto;
}
.sticky-grid .wpb_wrapper > section > .cta-box, .sticky-grid .wpb_wrapper > section > .cta-box-a > .cta-box, .sticky-grid .wpb_wrapper > section > .cta-image, .sticky-grid .wpb_wrapper > section > .cta-video, .sticky-grid .wpb_wrapper > section > .cta-icon-text, .space-grid .wpb_wrapper > section > .cta-box, .space-grid .wpb_wrapper > section > .cta-box-a > .cta-box, .space-grid .wpb_wrapper > section > .cta-image, .space-grid .wpb_wrapper > section > .cta-video, .space-grid .wpb_wrapper > section > .cta-icon-text {

flex:1;
	height: auto;
}
	.vc_row.vc_row-flex {
    width: 100%;
}
	.cta-icon-text {
	display: flex;
     flex-wrap: wrap;
		
	}
	
	.cta-icon-text h4 {
		
     width: 100%;
	}
	
	  @supports (overflow: -webkit-marquee) and (justify-content: inherit) {
    /* line 256, ../scss/breakpoints/_tablet-up.scss */
    .sticky-grid,
    .space-grid {
	display: -webkit-box !important;
	display: -webkit-flex !important;
	display: -ms-flexbox !important;
	display: flex !important;
    min-height: initial !important;
    }
  }
	
	@media screen and (-ms-high-contrast: active), (-ms-high-contrast: none) {
    .sticky-grid .wpb_wrapper, .space-grid .wpb_wrapper {
		display: block;
		flex: auto;
	}
		.sticky-grid .wpb_wrapper > section, .space-grid .wpb_wrapper > section {
			height: 100%;
		}
	.cta-icon-text {
		display: block;
     }

	}
	
	html[data-useragent*='MSIE 10.0'] .vc_row.vc_row-flex > .vc_column_container > .vc_column-inner {
	  display: block;
	}

		html[data-useragent*='MSIE 10.0'] .sticky-grid .wpb_wrapper > section > .cta-box, html[data-useragent*='MSIE 10.0'] .sticky-grid .wpb_wrapper > section > .cta-box-a > .cta-box, html[data-useragent*='MSIE 10.0'] .sticky-grid .wpb_wrapper > section > .cta-image, .sticky-grid .wpb_wrapper > section > .cta-video, html[data-useragent*='MSIE 10.0'] .sticky-grid .wpb_wrapper > section > .cta-icon-text, .space-grid .wpb_wrapper > section > .cta-box, html[data-useragent*='MSIE 10.0'] .space-grid .wpb_wrapper > section > .cta-box-a > .cta-box, .space-grid .wpb_wrapper > section > .cta-image, html[data-useragent*='MSIE 10.0'] .space-grid .wpb_wrapper > section > .cta-video, html[data-useragent*='MSIE 10.0'] .space-grid .wpb_wrapper > section > .cta-icon-text {
	height: 100%;
	}
	
	html[data-useragent*='MSIE 10.0'] .vc_row.vc_row-flex {
    margin-bottom: 15px;
}
</style>

<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
<script>
window.addEventListener("load", function(){
window.cookieconsent.initialise({
  "palette": {
    "popup": {
      "background": "#20ACE0"
    },
    "button": {
      "background": "#FFFFFF"
    }
  }
})});
</script>

	</head>
	
	<body <?php body_class(); ?> itemscope itemtype="http://schema.org/WebPage">

		<div id="container">

			<header class="header" role="banner" itemscope itemtype="http://schema.org/WPHeader">

				<div class="top-row">
					<div class="column row">
						<div class="top-row-left">

						</div>
						<div class="top-row-right">
							<?php echo do_shortcode('[social-links]'); ?>
						</div>
					</div>
				</div>

				<div class="column row">

					<div id="mob-header">
						
						<div class="contact-info">
							<a href="tel:<?php echo get_theme_mod( 'bones_contact_tel' ); ?>"><i class="fa fa-phone" aria-hidden="true"></i></a>
							<a href="mailto:<?php echo get_theme_mod( 'bones_contact_email' ); ?>"><i class="fa fa-envelope" aria-hidden="true"></i></a>
						</div>

						<div class="logo">
							<a class="logo" href="<?php echo home_url(); ?>" rel="nofollow" title="Clear Marketing">
							<!-- Generator: Adobe Illustrator 21.0.2, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
							<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
								 viewBox="0 0 164 167" style="enable-background:new 0 0 164 167;" xml:space="preserve">
							<path d="M99.7,88.7c-0.6,0-1.3,0-1.9,0.1c-0.7,0.1-1.3,0.3-1.9,0.5c-0.6,0.3-1,0.7-1.4,1.1c-0.4,0.5-0.6,1.1-0.6,1.9
								c0,1.5,0.5,2.5,1.4,3c0.9,0.9,2.2,1.1,3.8,1.1c1,0,1.8,0,2.5-0.1c0.7,0,1.4-0.1,1.9-0.2v-6.8c-0.3-0.1-0.8-0.3-1.4-0.4
								C101.4,88.8,100.7,88.7,99.7,88.7z"/>
							<path d="M76.1,79.5c-0.9,0-1.8,0.2-2.5,0.5c-0.7,0.4-1.3,0.8-1.8,1.4s-0.9,1.2-1.2,2c-0.3,0.8-0.5,1.5-0.5,2.3h11.3
								c0-1.8-0.4-3.2-1.4-4.4C79.1,80.1,77.8,79.5,76.1,79.5z"/>
							<path d="M80.3,22c-35.1,0-63.6,28.5-63.6,63.6s28.5,63.6,63.6,63.6s63.6-28.5,63.6-63.6S115.4,22,80.3,22z M51,99
								c-0.4,0.1-0.9,0.2-1.5,0.3c-0.6,0.1-1.1,0.2-1.8,0.3c-0.8,0.2-1.4,0.3-1.9,0.3c-1.8,0-3.4-0.3-4.8-0.9c-1.4-0.6-2.5-1.4-3.5-2.4
								c-0.9-1-1.6-2.3-2.1-3.7s-0.7-3-0.7-4.8c0-1.7,0.3-3.3,0.8-4.8s1.2-2.7,2.2-3.8c0.9-1.1,2.1-1.9,3.4-2.5c1.4-0.6,2.9-0.9,4.5-0.9
								c1,0,2,0.1,3.1,0.3c1,0.2,2,0.5,2.9,0.8l-0.9,3.4c-0.6-0.3-1.3-0.5-2.1-0.7c-0.8-0.2-1.6-0.3-2.5-0.3c-2.3,0-4,0.7-5.2,2.1
								c-1.2,1.4-1.8,3.5-1.8,6.2c0,1.2,0.1,2.3,0.4,3.4c0.3,1,0.7,1.9,1.3,2.6c0.6,0.7,1.4,1.3,2.3,1.7c0.9,0.4,2.1,0.6,3.4,0.6
								c1.1,0,2-0.1,2.9-0.3c0.9-0.2,1.6-0.4,2-0.7l0.6,3.4C51.8,98.7,51.4,98.9,51,99z M63,99.8c-2.5-0.1-4.3-0.6-5.3-1.6
								s-1.6-2.6-1.6-4.8V66.2l4.1-0.7v27.2c0,0.7,0.1,1.2,0.2,1.7c0.1,0.4,0.3,0.8,0.6,1c0.3,0.3,0.6,0.5,1,0.6c0.4,0.1,1,0.2,1.6,0.3
								L63,99.8z M85.5,88.5c0,0.3,0,0.5,0,0.8H70c0.2,2.4,0.9,4.1,2,5.4c1.2,1.2,3.1,1.8,5.6,1.8c1.4,0,2.6-0.1,3.6-0.4
								c1-0.2,1.7-0.5,2.2-0.7l0.6,3.4c-0.5,0.3-1.4,0.5-2.6,0.8c-1.2,0.3-2.6,0.4-4.2,0.4c-2,0-3.7-0.3-5.1-0.9c-1.4-0.6-2.6-1.4-3.6-2.5
								c-0.9-1-1.6-2.3-2.1-3.7c-0.4-1.5-0.6-3.1-0.6-4.8c0-2,0.3-3.8,0.9-5.3s1.4-2.7,2.3-3.7c1-1,2.1-1.7,3.3-2.2
								c1.2-0.5,2.5-0.7,3.8-0.7c3.1,0,5.4,1,7,2.9c1.6,1.9,2.4,4.8,2.4,8.7C85.5,88,85.5,88.2,85.5,88.5z M107.5,99
								c-0.3,0.1-0.8,0.1-1.5,0.2c-0.6,0.1-1.3,0.2-2.1,0.3c-0.8,0.1-1.6,0.2-2.5,0.2c-0.9,0.1-1.8,0.1-2.7,0.1c-1.3,0-2.5-0.1-3.5-0.4
								c-1.1-0.3-2-0.7-2.8-1.2c-0.8-0.6-1.4-1.3-1.8-2.2c-0.4-0.9-0.7-2-0.7-3.4c0-1.2,0.3-2.3,0.8-3.2s1.2-1.6,2.1-2.2
								c0.9-0.6,1.9-1,3.1-1.2c1.2-0.3,2.4-0.4,3.7-0.4c0.4,0,0.8,0,1.3,0.1c0.4,0,0.8,0.1,1.2,0.2c0.4,0.1,0.7,0.1,1,0.2
								c0.3,0.1,0.5,0.1,0.6,0.1V85c0-0.7-0.1-1.3-0.2-2c-0.1-0.7-0.4-1.2-0.8-1.7c-0.4-0.5-0.9-0.9-1.5-1.2c-0.7-0.3-1.5-0.5-2.5-0.5
								c-1.3,0-2.5,0.1-3.5,0.3s-1.8,0.4-2.2,0.6l-0.5-3.4c0.5-0.2,1.4-0.5,2.6-0.7c0.9-0.2,2.2-0.3,3.7-0.3c1.6,0,3,0.2,4.1,0.6
								c1.1,0.4,2,1,2.7,1.8c0.7,0.8,1.2,1.7,1.5,2.7c0.3,1.1,0.4,2.2,0.4,3.5V99z M123.7,80c-0.7-0.2-1.7-0.2-2.9-0.2
								c-0.8,0-1.5,0.1-2.2,0.2c-0.7,0.2-1.2,0.3-1.5,0.3v19H113V77.7c1-0.3,2.1-0.7,3.6-1c1.6-0.3,3.2-0.5,4.9-0.5c0.3,0,0.7,0,1.2,0.1
								c0.5,0,0.9,0.1,1.3,0.2c0.4,0.1,0.8,0.1,1.2,0.2c0.4,0.1,0.6,0.1,0.8,0.2l-0.7,3.5C125,80.3,124.5,80.1,123.7,80z"/>
							<path d="M81.4,85.8v-0.1h0C81.4,85.7,81.4,85.8,81.4,85.8z"/>
							</svg>
							</a>
						</div>

						<div>
							<div id="mobNavBurgerMenu">

								<nav itemscope itemtype="http://schema.org/SiteNavigationElement">
									<input type="checkbox" id="menu-toggle"/>
									<label id="trigger" for="menu-toggle"></label>
									<label id="burger" for="menu-toggle"></label>
									<?php wp_nav_menu(array(
			    					         'container' => false,                           // remove nav container
			    					         'container_class' => 'menu',                 // class of container (should you choose to use it)
			    					         'menu' => __( 'The Main Menu', 'bonestheme' ),  // nav name
			    					         'menu_class' => 'nav top-nav',               // adding custom nav class
			    					         'theme_location' => 'main-nav',                 // where it's located in the theme
			    					         'before' => '',                                 // before the menu
			        			               'after' => '',                                  // after the menu
			        			               'link_before' => '',                            // before each link
			        			               'link_after' => '',                             // after each link
			        			               'depth' => 0,                                   // limit the depth of the nav
			    					         'fallback_cb' => ''                             // fallback function (if there is one)
									)); ?>

								</nav>

							</div>
						</div>

					</div>

				</div>

				<div id="desk-header" class="column row">

					<div class="top-row-left">

						<a class="logo" href="<?php echo home_url(); ?>" rel="nofollow" title="Clear Marketing">

						<!-- Generator: Adobe Illustrator 21.0.2, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
						<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							 viewBox="0 0 164 167" style="enable-background:new 0 0 164 167;" xml:space="preserve">
						<path d="M99.7,88.7c-0.6,0-1.3,0-1.9,0.1c-0.7,0.1-1.3,0.3-1.9,0.5c-0.6,0.3-1,0.7-1.4,1.1c-0.4,0.5-0.6,1.1-0.6,1.9
							c0,1.5,0.5,2.5,1.4,3c0.9,0.9,2.2,1.1,3.8,1.1c1,0,1.8,0,2.5-0.1c0.7,0,1.4-0.1,1.9-0.2v-6.8c-0.3-0.1-0.8-0.3-1.4-0.4
							C101.4,88.8,100.7,88.7,99.7,88.7z"/>
						<path d="M76.1,79.5c-0.9,0-1.8,0.2-2.5,0.5c-0.7,0.4-1.3,0.8-1.8,1.4s-0.9,1.2-1.2,2c-0.3,0.8-0.5,1.5-0.5,2.3h11.3
							c0-1.8-0.4-3.2-1.4-4.4C79.1,80.1,77.8,79.5,76.1,79.5z"/>
						<path d="M80.3,22c-35.1,0-63.6,28.5-63.6,63.6s28.5,63.6,63.6,63.6s63.6-28.5,63.6-63.6S115.4,22,80.3,22z M51,99
							c-0.4,0.1-0.9,0.2-1.5,0.3c-0.6,0.1-1.1,0.2-1.8,0.3c-0.8,0.2-1.4,0.3-1.9,0.3c-1.8,0-3.4-0.3-4.8-0.9c-1.4-0.6-2.5-1.4-3.5-2.4
							c-0.9-1-1.6-2.3-2.1-3.7s-0.7-3-0.7-4.8c0-1.7,0.3-3.3,0.8-4.8s1.2-2.7,2.2-3.8c0.9-1.1,2.1-1.9,3.4-2.5c1.4-0.6,2.9-0.9,4.5-0.9
							c1,0,2,0.1,3.1,0.3c1,0.2,2,0.5,2.9,0.8l-0.9,3.4c-0.6-0.3-1.3-0.5-2.1-0.7c-0.8-0.2-1.6-0.3-2.5-0.3c-2.3,0-4,0.7-5.2,2.1
							c-1.2,1.4-1.8,3.5-1.8,6.2c0,1.2,0.1,2.3,0.4,3.4c0.3,1,0.7,1.9,1.3,2.6c0.6,0.7,1.4,1.3,2.3,1.7c0.9,0.4,2.1,0.6,3.4,0.6
							c1.1,0,2-0.1,2.9-0.3c0.9-0.2,1.6-0.4,2-0.7l0.6,3.4C51.8,98.7,51.4,98.9,51,99z M63,99.8c-2.5-0.1-4.3-0.6-5.3-1.6
							s-1.6-2.6-1.6-4.8V66.2l4.1-0.7v27.2c0,0.7,0.1,1.2,0.2,1.7c0.1,0.4,0.3,0.8,0.6,1c0.3,0.3,0.6,0.5,1,0.6c0.4,0.1,1,0.2,1.6,0.3
							L63,99.8z M85.5,88.5c0,0.3,0,0.5,0,0.8H70c0.2,2.4,0.9,4.1,2,5.4c1.2,1.2,3.1,1.8,5.6,1.8c1.4,0,2.6-0.1,3.6-0.4
							c1-0.2,1.7-0.5,2.2-0.7l0.6,3.4c-0.5,0.3-1.4,0.5-2.6,0.8c-1.2,0.3-2.6,0.4-4.2,0.4c-2,0-3.7-0.3-5.1-0.9c-1.4-0.6-2.6-1.4-3.6-2.5
							c-0.9-1-1.6-2.3-2.1-3.7c-0.4-1.5-0.6-3.1-0.6-4.8c0-2,0.3-3.8,0.9-5.3s1.4-2.7,2.3-3.7c1-1,2.1-1.7,3.3-2.2
							c1.2-0.5,2.5-0.7,3.8-0.7c3.1,0,5.4,1,7,2.9c1.6,1.9,2.4,4.8,2.4,8.7C85.5,88,85.5,88.2,85.5,88.5z M107.5,99
							c-0.3,0.1-0.8,0.1-1.5,0.2c-0.6,0.1-1.3,0.2-2.1,0.3c-0.8,0.1-1.6,0.2-2.5,0.2c-0.9,0.1-1.8,0.1-2.7,0.1c-1.3,0-2.5-0.1-3.5-0.4
							c-1.1-0.3-2-0.7-2.8-1.2c-0.8-0.6-1.4-1.3-1.8-2.2c-0.4-0.9-0.7-2-0.7-3.4c0-1.2,0.3-2.3,0.8-3.2s1.2-1.6,2.1-2.2
							c0.9-0.6,1.9-1,3.1-1.2c1.2-0.3,2.4-0.4,3.7-0.4c0.4,0,0.8,0,1.3,0.1c0.4,0,0.8,0.1,1.2,0.2c0.4,0.1,0.7,0.1,1,0.2
							c0.3,0.1,0.5,0.1,0.6,0.1V85c0-0.7-0.1-1.3-0.2-2c-0.1-0.7-0.4-1.2-0.8-1.7c-0.4-0.5-0.9-0.9-1.5-1.2c-0.7-0.3-1.5-0.5-2.5-0.5
							c-1.3,0-2.5,0.1-3.5,0.3s-1.8,0.4-2.2,0.6l-0.5-3.4c0.5-0.2,1.4-0.5,2.6-0.7c0.9-0.2,2.2-0.3,3.7-0.3c1.6,0,3,0.2,4.1,0.6
							c1.1,0.4,2,1,2.7,1.8c0.7,0.8,1.2,1.7,1.5,2.7c0.3,1.1,0.4,2.2,0.4,3.5V99z M123.7,80c-0.7-0.2-1.7-0.2-2.9-0.2
							c-0.8,0-1.5,0.1-2.2,0.2c-0.7,0.2-1.2,0.3-1.5,0.3v19H113V77.7c1-0.3,2.1-0.7,3.6-1c1.6-0.3,3.2-0.5,4.9-0.5c0.3,0,0.7,0,1.2,0.1
							c0.5,0,0.9,0.1,1.3,0.2c0.4,0.1,0.8,0.1,1.2,0.2c0.4,0.1,0.6,0.1,0.8,0.2l-0.7,3.5C125,80.3,124.5,80.1,123.7,80z"/>
						<path d="M81.4,85.8v-0.1h0C81.4,85.7,81.4,85.8,81.4,85.8z"/>
						</svg>
						</a>

						<div class="header-telephone">
							<i class="fa fa-phone" aria-hidden="true"></i> Call us today:
							<?php
							if ( get_theme_mod( 'bones_contact_tel' ) ) { ?>
								<span class="tel"><?php echo get_theme_mod( 'bones_contact_tel' ); ?> (Manchester)</span>
							<?php } ?>

							<?php
							if ( get_theme_mod( 'bones_contact_tel_two' ) ) { ?>
								| <span class="tel"><?php echo get_theme_mod( 'bones_contact_tel_two' ); ?> (Glasgow)</span>
							<?php } ?>
						</div>

					</div>

					<div class="top-row-right">

						<div class="burger-menu-holder">
							
							<div class="burger-menu">
							  <div class="burger"></div>  
							</div>

							<div id="mainNavBurgerMenu">

								<nav itemscope itemtype="http://schema.org/SiteNavigationElement">
									<?php wp_nav_menu(array(
			    					         'container' => false,                           // remove nav container
			    					         'container_class' => 'menu',                 // class of container (should you choose to use it)
			    					         'menu' => __( 'The Main Menu', 'bonestheme' ),  // nav name
			    					         'menu_class' => 'nav top-nav',               // adding custom nav class
			    					         'theme_location' => 'main-nav',                 // where it's located in the theme
			    					         'before' => '',                                 // before the menu
			        			               'after' => '',                                  // after the menu
			        			               'link_before' => '',                            // before each link
			        			               'link_after' => '',                             // after each link
			        			               'depth' => 0,                                   // limit the depth of the nav
			    					         'fallback_cb' => ''                             // fallback function (if there is one)
									)); ?>

								</nav>

							</div>

						</div>

					</div>

				</div>

			</header>
