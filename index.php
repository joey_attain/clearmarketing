<?php 
if ( ! defined( 'ABSPATH' ) ) {
	die('Ah ah ah... No script kiddies!');
}
get_header();

?>

<div class="page-banner" style="background-image: url(/wp-content/uploads/our-blog-banner.jpg);">
    <div class="breadcrumbs-bar">
	    <div class="column row">
		    <?php
			    if ( function_exists('yoast_breadcrumb') ) {
			    yoast_breadcrumb('
			    <p id="breadcrumbs">','</p>
			    ');
		    }
	    ?>
	    </div>
    </div>
    <div class="column row">
        <h1 class="page-banner-heading">See what's going on in Our Blog</h1>
    </div>
</div>

	<div id="content">

		<div id="inner-content" class="row">

				<main id="main" class="medium-8 column" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

					<div class="row">

					<?php

					$sticky = get_option( 'sticky_posts' );
					$args = array(
						'posts_per_page' => 1,
						'post__in'  => $sticky,
						'ignore_sticky_posts' => 1
					);
					$query = new WP_Query( $args );
					if ( isset($sticky[0]) ) {
						 while ($query->have_posts()) { $query->the_post();
						 
							get_template_part('templates/entry', 'post-sticky');

						}
					}

	                $postcount = 1; // set 0 with featured sticky
	            	?>

					<?php if ($wp_query->have_posts()) {
						 while ($wp_query->have_posts()) { $wp_query->the_post();

							// if($postcount % 2 != 0) { echo '</div>'; }
						 //    $postcount++;
						 //    if($postcount % 2 == 0) { echo '<div class="row">'; }
						 
							get_template_part('templates/entry', 'post-sticky');

						} ?>

						</div>

						<?php bones_page_navi(); ?>

					<?php } else { ?>
						<h2>Sorry!</h2>
						<p>There are currently no posts. Check back soon!</p>
					<?php } ?>

					<?php wp_reset_postdata(); ?>

				</main>

			<?php get_sidebar(); ?>

		</div>

	</div>

<?php echo do_shortcode('[testimonial-slider]'); ?>
<?php get_footer(); ?>
