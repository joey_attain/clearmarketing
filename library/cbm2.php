<?php

add_action( 'cmb2_admin_init', 'cmb2_page_metaboxes' );
add_action( 'cmb2_admin_init', 'cmb2_testimonial_metaboxes' );
add_action( 'cmb2_admin_init', 'cmb2_pagebanner_metaboxes' );
add_action( 'cmb2_admin_init', 'cmb2_job_metaboxes' );
add_action( 'cmb2_admin_init', 'cmb2_work_metaboxes' );


//page metaboxes
function cmb2_page_metaboxes() {

  // Start with an underscore to hide fields from custom fields list
  $prefix = 'titleBanner_';

  /**
   * Initiate the metabox
   */
  $cmb_titleBanner = new_cmb2_box( array(
    'id'            => 'titleBanner',
    'title'         => __( 'Banner With Title', 'cmb2' ),
    'object_types'  => array( 'page', 'work' ), // Post type
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true, // Show field names on the left
  ) );

  $cmb_titleBanner->add_field( array(
    'name' => 'Show Home Banners Slider',
    'desc' => 'Adds large banner at the top of the page',
    'id'   => 'homebanner_checkbox',
    'type' => 'checkbox',
  ) );

  $cmb_titleBanner->add_field( array(
    'name' => 'Show Banner',
    'id'   => $prefix . 'checkbox',
    'type' => 'checkbox',
  ) );

  $cmb_titleBanner->add_field( array(
    'name' => esc_html__( 'Banner Heading', 'cmb2' ),
    'desc' => esc_html__( 'Heading in Banner', 'cmb2' ),
    'id'   => $prefix . 'heading',
    'type' => 'text',
  ) );

  $cmb_titleBanner->add_field( array(
    'name' => esc_html__( 'Text to make Bold', 'cmb2' ),
    'desc' => esc_html__( 'Enter what word(s) in the heading you want to make bold (Case Sensitive!)', 'cmb2' ),
    'id'   => $prefix . 'heading_bold',
    'type' => 'text',
  ) );

  $cmb_titleBanner->add_field( array(
    'name'    => 'Banner Background Image',
    'desc'    => 'Background Image for banner (Will not show on mobile devices)',
    'id'      => $prefix . 'image',
    'type'    => 'file',
    // Optional:
    'options' => array(
      'url' => false, // Hide the text input for the url
    ),
    'text'    => array(
      'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
    ),
  ) );

  $cmb_titleBanner->add_field( array(
    'name'    => 'Banner Background Colour',
    'desc'    => 'Background colour for banner (Will show on mobile devices and banners without background images)',
    'id'      => $prefix . 'color',
    'type'    => 'colorpicker',
    'default' => '#323944',
  ) );

}

/**
 * Define the metabox and field configurations.
 */
function cmb2_testimonial_metaboxes() {

  // Start with an underscore to hide fields from custom fields list
  $prefix = 'testimonial_';

  /**
   * Initiate the metabox
   */
  $cmb_testimonial = new_cmb2_box( array(
    'id'            => 'testimonial_content',
    'title'         => __( 'Testimonial Content', 'cmb2' ),
    'object_types'  => array( 'testimonial' ), // Post type
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true, // Show field names on the left
    // 'cmb_styles' => false, // false to disable the CMB stylesheet
    // 'closed'     => true, // Keep the metabox closed by default
  ) );

  $cmb_testimonial->add_field( array(
      'name' => esc_html__( 'Name', 'cmb2' ),
      'desc' => esc_html__( 'Name of testimonail giver', 'cmb2' ),
      'id'   => $prefix . 'name',
      'type' => 'text',
    ) );

  $cmb_testimonial->add_field( array(
      'name' => esc_html__( 'Person Title', 'cmb2' ),
      'desc' => esc_html__( 'Title of person', 'cmb2' ),
      'id'   => $prefix . 'desc',
      'type' => 'text',
    ) );

  $cmb_testimonial->add_field( array(
    'name'    => 'Colour Scheme 1',
    'desc'    => 'Background colour',
    'id'      => $prefix . 'color_scheme_1',
    'type'    => 'colorpicker',
    'default' => '#f4f4f2',
  ) );

  $cmb_testimonial->add_field( array(
    'name'    => 'Colour Scheme 2',
    'desc'    => 'Elements colour',
    'id'      => $prefix . 'color_scheme_2',
    'type'    => 'colorpicker',
    'default' => '#323944',
  ) );

  // Add other metaboxes as needed

}


function cmb2_pagebanner_metaboxes() {

  /**
   * Initiate the metabox
   */
  $cmb_pagebanner = new_cmb2_box( array(
    'id'            => 'pagebanner_elements',
    'title'         => __( 'Banner Elements', 'cmb2' ),
    'object_types'  => array( 'homebanner' ), // Post type
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true, // Show field names on the left
  ) );

  $cmb_pagebanner->add_field( array(
      'name' => esc_html__( 'Banner Heading', 'cmb2' ),
      'desc' => esc_html__( 'Heading in Banner', 'cmb2' ),
      'id'   => 'homebanner_heading',
      'type' => 'text',
    ) );

  $cmb_pagebanner->add_field( array(
    'name'    => 'Banner Image',
    'desc'    => 'Upload an image or enter an URL.',
    'id'      => 'homebanner_image',
    'type'    => 'file',
    // Optional:
    'options' => array(
      'url' => false, // Hide the text input for the url
    ),
    'text'    => array(
      'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
    ),
  ) );

  $cmb_pagebanner->add_field( array(
      'name' => esc_html__( 'Button Text', 'cmb2' ),
      'desc' => esc_html__( 'Text for button', 'cmb2' ),
      'id'   => 'homebanner_button_text',
      'type' => 'text',
    ) );
  
  $cmb_pagebanner->add_field( array(
    'name' => __( 'Button URL', 'cmb2' ),
    'id'   => 'homebanner_button_url',
    'type' => 'text_url',
  ) );

  $cmb_pagebanner->add_field( array(
      'name' => esc_html__( 'Youtube Embed', 'cmb2' ),
      'desc' => esc_html__( 'Enter video embed link', 'cmb2' ),
      'id'   => 'homebanner_youtube_video',
      'type' => 'text',
    ) );

  $cmb_pagebanner->add_field( array(
      'name' => esc_html__( 'Video .mp4', 'cmb2' ),
      'desc' => esc_html__( 'Enter video .mp4 link', 'cmb2' ),
      'id'   => 'homebanner_full_width_video_mp4',
      'type' => 'text',
    ) );

  $cmb_pagebanner->add_field( array(
      'name' => esc_html__( 'Video .webm', 'cmb2' ),
      'desc' => esc_html__( 'Enter video .webm link', 'cmb2' ),
      'id'   => 'homebanner_full_width_video_webm',
      'type' => 'text',
    ) );

  $cmb_pagebanner->add_field( array(
      'name' => esc_html__( 'Video image', 'cmb2' ),
      'desc' => esc_html__( 'Enter video image link', 'cmb2' ),
      'id'   => 'homebanner_full_width_video_image',
      'type' => 'text',
    ) );

}

/**
 * Define the metabox and field configurations.
 */
function cmb2_job_metaboxes() {

  // Start with an underscore to hide fields from custom fields list
  $prefix = 'job_';

  /**
   * Initiate the metabox
   */
  $cmb_job = new_cmb2_box( array(
    'id'            => 'job_content',
    'title'         => __( 'Job Content', 'cmb2' ),
    'object_types'  => array( 'job' ), // Post type
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true, // Show field names on the left
    // 'cmb_styles' => false, // false to disable the CMB stylesheet
    // 'closed'     => true, // Keep the metabox closed by default
  ) );

  $cmb_job->add_field( array(
      'name' => esc_html__( 'Salary', 'cmb2' ),
      'desc' => esc_html__( 'Salary of job', 'cmb2' ),
      'id'   => $prefix . 'salary',
      'type' => 'text',
    ) );

  $cmb_job->add_field( array(
      'name' => esc_html__( 'Location', 'cmb2' ),
      'desc' => esc_html__( 'Location of job (e.g. Manchester)', 'cmb2' ),
      'id'   => $prefix . 'loc',
      'type' => 'text',
    ) );

  $cmb_job->add_field( array(
      'name' => esc_html__( 'Position', 'cmb2' ),
      'desc' => esc_html__( 'e.g. Full time or part time', 'cmb2' ),
      'id'   => $prefix . 'pos',
      'type' => 'text',
    ) );

  $cmb_job->add_field( array(
    'name'    => 'Color Scheme',
    'desc'    => 'Color of elements',
    'id'      => $prefix . 'colorScheme',
    'type'    => 'colorpicker',
    'default' => '#20ace0',
  ) );

  // Add other metaboxes as needed

}

/**
 * Define the metabox and field configurations.
 */
function cmb2_work_metaboxes() {

  // Start with an underscore to hide fields from custom fields list
  $prefix = 'work_';

  /**
   * Initiate the metabox
   */
  $cmb_work = new_cmb2_box( array(
    'id'            => 'work_content',
    'title'         => __( 'Work Content', 'cmb2' ),
    'object_types'  => array( 'work' ), // Post type
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true, // Show field names on the left
    // 'cmb_styles' => false, // false to disable the CMB stylesheet
    // 'closed'     => true, // Keep the metabox closed by default
  ) );

  $cmb_work->add_field( array(
      'name' => esc_html__( 'Project Name', 'cmb2' ),
      'id'   => $prefix . 'company_name',
      'type' => 'text'
    ) );

  // Add other metaboxes as needed

}

?>