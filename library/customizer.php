<?php
/************* THEME CUSTOMIZE *********************/

/* 
  A good tutorial for creating your own Sections, Controls and Settings:
  http://code.tutsplus.com/series/a-guide-to-the-wordpress-theme-customizer--wp-33722
  
  Good articles on modifying the default options:
  http://natko.com/changing-default-wordpress-theme-customization-api-sections/
  http://code.tutsplus.com/tutorials/digging-into-the-theme-customizer-components--wp-27162
  
  To do:
  - Create a js for the postmessage transport method
  - Create some sanitize functions to sanitize inputs
  - Create some boilerplate Sections, Controls and Settings
*/

function bones_theme_customizer($wp_customize) {
  // $wp_customize calls go here.

/*********************
SOCIAL LINKS
*********************/

  $wp_customize->add_section( 'social_links' , array(
      'title'      => __('Social Links','bones'),
      'priority'   => 30,
  ) );

  $wp_customize->add_setting( 'bones_social_facebook' );
  $wp_customize->add_setting( 'bones_social_twitter' );
  $wp_customize->add_setting( 'bones_social_linkedin' );
  $wp_customize->add_setting( 'bones_social_youtube' );
  $wp_customize->add_setting( 'bones_social_googlePlus' );

  $wp_customize->add_control(
      new WP_Customize_Control(
          $wp_customize,
          'social_facebook',
          array(
              'label'      => __( 'Facebook Link', 'bones' ),
              'section'    => 'social_links',
              'settings'   => 'bones_social_facebook',
              'type'       => 'text'
          )
      )
  );

  $wp_customize->add_control(
      new WP_Customize_Control(
          $wp_customize,
          'social_twitter',
          array(
              'label'      => __( 'Twitter Link', 'bones' ),
              'section'    => 'social_links',
              'settings'   => 'bones_social_twitter',
              'type'       => 'text'
          )
      )
  );

  $wp_customize->add_control(
      new WP_Customize_Control(
          $wp_customize,
          'social_linkedin',
          array(
              'label'      => __( 'LinkedIn Link', 'bones' ),
              'section'    => 'social_links',
              'settings'   => 'bones_social_linkedin',
              'type'       => 'text'
          )
      )
  );

  $wp_customize->add_control(
      new WP_Customize_Control(
          $wp_customize,
          'social_youtube',
          array(
              'label'      => __( 'Youtube Link', 'bones' ),
              'section'    => 'social_links',
              'settings'   => 'bones_social_youtube',
              'type'       => 'text'
          )
      )
  );

  $wp_customize->add_control(
      new WP_Customize_Control(
          $wp_customize,
          'social_googlePlus',
          array(
              'label'      => __( 'Google Plus Link', 'bones' ),
              'section'    => 'social_links',
              'settings'   => 'bones_social_googlePlus',
              'type'       => 'text'
          )
      )
  );

/*********************
CONTACT DETAILS
*********************/

  $wp_customize->add_section( 'contact_info' , array(
      'title'      => __('Contact Information','bones'),
      'priority'   => 30,
  ) );

  $wp_customize->add_setting( 'bones_contact_email' );
  $wp_customize->add_setting( 'bones_contact_email_jim' );
  $wp_customize->add_setting( 'bones_contact_email_elaine' );
  $wp_customize->add_setting( 'bones_contact_email_matthew' );
  $wp_customize->add_setting( 'bones_contact_email_gemma' );
  $wp_customize->add_setting( 'bones_contact_tel' );
  $wp_customize->add_setting( 'bones_contact_tel_two' );
  $wp_customize->add_setting( 'bones_contact_addr' );
  $wp_customize->add_setting( 'bones_contact_addr_two' );

  $wp_customize->add_control(
      new WP_Customize_Control(
          $wp_customize,
          'contact_email',
          array(
              'label'      => __( 'Email Address', 'bones' ),
              'section'    => 'contact_info',
              'settings'   => 'bones_contact_email',
              'type'       => 'text',
              'description' => 'Type [email] to display on your page'
          )
      )
  );

  $wp_customize->add_control(
      new WP_Customize_Control(
          $wp_customize,
          'contact_email_jim',
          array(
              'label'      => __( 'Email Address (Jim)', 'bones' ),
              'section'    => 'contact_info',
              'settings'   => 'bones_contact_email_jim',
              'type'       => 'text',
              'description' => 'Type [email-jim] to display on your page'
          )
      )
  );

  $wp_customize->add_control(
      new WP_Customize_Control(
          $wp_customize,
          'contact_email_elaine',
          array(
              'label'      => __( 'Email Address (Elaine)', 'bones' ),
              'section'    => 'contact_info',
              'settings'   => 'bones_contact_email_elaine',
              'type'       => 'text',
              'description' => 'Type [email-elaine] to display on your page'
          )
      )
  );

  $wp_customize->add_control(
      new WP_Customize_Control(
          $wp_customize,
          'contact_email_matthew',
          array(
              'label'      => __( 'Email Address (Matthew)', 'bones' ),
              'section'    => 'contact_info',
              'settings'   => 'bones_contact_email_matthew',
              'type'       => 'text',
              'description' => 'Type [email-matthew] to display on your page'
          )
      )
  );

  $wp_customize->add_control(
      new WP_Customize_Control(
          $wp_customize,
          'contact_email_gemma',
          array(
              'label'      => __( 'Email Address (Gemma)', 'bones' ),
              'section'    => 'contact_info',
              'settings'   => 'bones_contact_email_gemma',
              'type'       => 'text',
              'description' => 'Type [email-gemma] to display on your page'
          )
      )
  );

  $wp_customize->add_control(
      new WP_Customize_Control(
          $wp_customize,
          'contact_tel',
          array(
              'label'      => __( 'Telephone Number (Manchester)', 'bones' ),
              'section'    => 'contact_info',
              'settings'   => 'bones_contact_tel',
              'type'       => 'text',
              'description' => 'Type [telephone-manchester] to display on your page'
          )
      )
  );

  $wp_customize->add_control(
      new WP_Customize_Control(
          $wp_customize,
          'contact_tel_two',
          array(
              'label'      => __( 'Telephone Number (Glasgow)', 'bones' ),
              'section'    => 'contact_info',
              'settings'   => 'bones_contact_tel_two',
              'type'       => 'text',
              'description' => 'Type [telephone-glasgow] to display on your page'
          )
      )
  );

  $wp_customize->add_control(
      new WP_Customize_Control(
          $wp_customize,
          'contact_addr',
          array(
              'label'      => __( 'Address (Manchester)', 'bones' ),
              'section'    => 'contact_info',
              'settings'   => 'bones_contact_addr',
              'type'       => 'textarea',
              'description' => 'Type [address-manchester] to display on your page'
          )
      )
  );

  $wp_customize->add_control(
      new WP_Customize_Control(
          $wp_customize,
          'contact_addr_two',
          array(
              'label'      => __( 'Address (Glasgow)', 'bones' ),
              'section'    => 'contact_info',
              'settings'   => 'bones_contact_addr_two',
              'type'       => 'textarea',
              'description' => 'Type [address-glasgow] to display on your page'
          )
      )
  );

/*********************
GOOGLE SYNCHRONIZATION SETTINGS
*********************/

  $wp_customize->add_section( 'google_sync' , array(
      'title'      => __('Google Synchronisation Settings','bones'),
      'priority'   => 30,
  ) );

  $wp_customize->add_setting( 'bones_google_tracking' );
  $wp_customize->add_setting( 'bones_google_captcha_siteKey' );
  $wp_customize->add_setting( 'bones_google_captcha_secret' );

  $wp_customize->add_control(
      new WP_Customize_Control(
          $wp_customize,
          'google_tracking',
          array(
              'label'      => __( 'Google Analytics Tracking Script', 'bones' ),
              'section'    => 'google_sync',
              'settings'   => 'bones_google_tracking',
              'type'       => 'textarea'
          )
      )
  );

  $wp_customize->add_control(
      new WP_Customize_Control(
          $wp_customize,
          'google_captcha_siteKey',
          array(
              'label'      => __( 'Google reCatpcha Site Key', 'bones' ),
              'section'    => 'google_sync',
              'settings'   => 'bones_google_captcha_siteKey',
              'type'       => 'text'
          )
      )
  );

  $wp_customize->add_control(
      new WP_Customize_Control(
          $wp_customize,
          'google_captcha_secret',
          array(
              'label'      => __( 'Google reCatpcha Secret Key', 'bones' ),
              'section'    => 'google_sync',
              'settings'   => 'bones_google_captcha_secret',
              'type'       => 'text'
          )
      )
  );

/*********************
SITE LOGO
*********************/

  $wp_customize->add_setting( 'bones_logo' );
  $wp_customize->add_setting( 'bones_logo_retina_width' );

  $wp_customize->add_control(
      new WP_Customize_Image_Control(
          $wp_customize,
          'logo',
          array(
              'label'       => __( 'Site Logo', 'bones' ),
              'section'     => 'title_tagline',
              'settings'    => 'bones_logo',
              'description' => 'To ensure the logo looks best on mobile devices, upload the logo twice the size as what it should be on dekstop devices, then type in half the size of the width in the Retina width box below.'
          )
      )
  );

  $wp_customize->add_control(
      new WP_Customize_Control(
          $wp_customize,
          'logo_retina_width',
          array(
              'label'      => __( 'Logo Retina Width', 'bones' ),
              'section'    => 'title_tagline',
              'settings'   => 'bones_logo_retina_width',
              'type'       => 'text',
              'description' => 'Type in half the width in px of the uploaded Retina logo here.'
          )
      )
  );

/*********************
COPYRIGHT TEXT
*********************/

  $wp_customize->add_setting( 'bones_copyright' );

  $wp_customize->add_control(
      new WP_Customize_Control(
          $wp_customize,
          'copyright',
          array(
              'label'       => __( 'Copyright Text', 'bones' ),
              'section'     => 'title_tagline',
              'settings'    => 'bones_copyright'
          )
      )
  );

  // Uncomment the below lines to remove the default customize sections 

  // $wp_customize->remove_section('title_tagline');
  $wp_customize->remove_section('colors');
  $wp_customize->remove_section('background_image');
  $wp_customize->remove_section('static_front_page');
  // $wp_customize->remove_section('nav');

  // Uncomment the below lines to remove the default controls
  // $wp_customize->remove_control('blogdescription');
  
  // Uncomment the following to change the default section titles
  // $wp_customize->get_section('colors')->title = __( 'Theme Colors' );
  // $wp_customize->get_section('background_image')->title = __( 'Images' );
}

add_action( 'customize_register', 'bones_theme_customizer' );

?>