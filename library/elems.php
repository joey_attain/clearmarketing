<?php
/************* DISPLAY ELEMENTS *********************/
/*
  All frontend elements, shortcodes and widgets/sidenars 
  to go in this file.
*/

/************* ACTIVE SIDEBARS ********************/

// Sidebars & Widgetizes Areas
function bones_register_sidebars() {
  register_sidebar(array(
    'id' => 'sidebar1',
    'name' => __( 'Sidebar 1', 'bonestheme' ),
    'description' => __( 'The first (primary) sidebar.', 'bonestheme' ),
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h4 class="widgettitle">',
    'after_title' => '</h4>',
  ));

  /*
  to add more sidebars or widgetized areas, just copy
  and edit the above sidebar code. In order to call
  your new sidebar just use the following code:

  Just change the name to whatever your new
  sidebar's id is, for example:

  register_sidebar(array(
    'id' => 'sidebar2',
    'name' => __( 'Sidebar 2', 'bonestheme' ),
    'description' => __( 'The second (secondary) sidebar.', 'bonestheme' ),
    'before_widget' => '<div id="%1$s" class="widget %2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h4 class="widgettitle">',
    'after_title' => '</h4>',
  ));

  To call the sidebar in your template, you can just copy
  the sidebar.php file and rename it to your sidebar's name.
  So using the above example, it would be:
  sidebar-sidebar2.php

  */
} // don't remove this bracket!

/************* SOCIAL LINKS *********************/
/*
  The values of these social links are set within the customizer (library/customizer.php)
*/
function get_socialLinks() {
  $html = '<span class="social-links">';
    if ( get_theme_mod( 'bones_social_facebook' ) ) {
      $html .= ' <a href="'.esc_url( get_theme_mod( 'bones_social_facebook' ) ).'" target="_blank" title="Facebook"><i class="fa fa-facebook" aria-hidden="true"></i><span class="sr-only">Facebook</span></a>';
    }
    if ( get_theme_mod( 'bones_social_twitter' ) ) {
      $html .= ' <a href="'.esc_url( get_theme_mod( 'bones_social_twitter' ) ).'" target="_blank" title="Twitter"><i class="fa fa-twitter" aria-hidden="true"></i><span class="sr-only">Facebook</span></a>';
    }
    if ( get_theme_mod( 'bones_social_linkedin' ) ) {
      $html .= ' <a href="'.esc_url( get_theme_mod( 'bones_social_linkedin' ) ).'" target="_blank" title="LinkedIn"><i class="fa fa-linkedin" aria-hidden="true"></i><span class="sr-only">Facebook</span></a>';
    }
    if ( get_theme_mod( 'bones_social_youtube' ) ) {
      $html .= ' <a href="'.esc_url( get_theme_mod( 'bones_social_youtube' ) ).'" target="_blank" title="Youtube"><i class="fa fa-youtube" aria-hidden="true"></i><span class="sr-only">Facebook</span></a>';
    }
    if ( get_theme_mod( 'bones_social_googlePlus' ) ) {
      $html .= ' <a href="'.esc_url( get_theme_mod( 'bones_social_googlePlus' ) ).'" target="_blank" title="Google Plus"><i class="fa fa-google-plus" aria-hidden="true"></i><span class="sr-only">Google Plus</span></a>';
    }
  $html .= '</span>';
  return $html;
}
add_shortcode( 'social-links', 'get_socialLinks' );

/************* CONTACT INFO SHORTCODES *********************/

function get_email() {
  return get_theme_mod( 'bones_contact_email' );
}
add_shortcode( 'email', 'get_email' );

function get_email_jim() {
  return get_theme_mod( 'bones_contact_email_jim' );
}
add_shortcode( 'email-jim', 'get_email_jim' );

function get_email_elaine() {
  return get_theme_mod( 'bones_contact_email_elaine' );
}
add_shortcode( 'email-elaine', 'get_email_elaine' );

function get_email_matthew() {
  return get_theme_mod( 'bones_contact_email_matthew' );
}
add_shortcode( 'email-matthew', 'get_email_matthew' );

function get_email_gemma() {
  return get_theme_mod( 'bones_contact_email_gemma' );
}
add_shortcode( 'email-gemma', 'get_email_gemma' );

function get_tel() {
  return get_theme_mod( 'bones_contact_tel' );;
}
add_shortcode( 'telephone-manchester', 'get_tel' );

function get_tel_two() {
  return get_theme_mod( 'bones_contact_tel_two' );;
}
add_shortcode( 'telephone-glasgow', 'get_tel_two' );

function get_address() {
  return get_theme_mod( 'bones_contact_addr' );;
}
add_shortcode( 'address-manchester', 'get_address' );

function get_address_two() {
  return get_theme_mod( 'bones_contact_addr_two' );;
}
add_shortcode( 'address-glasgow', 'get_address_two' );

/************* COMMENT LAYOUT *********************/

// Comment Layout
function bones_comments( $comment, $args, $depth ) {
   $GLOBALS['comment'] = $comment; ?>
  <div id="comment-<?php comment_ID(); ?>" <?php comment_class('cf'); ?>>
    <article  class="cf">
      <header class="comment-author vcard">
        <?php
        /*
          this is the new responsive optimized comment image. It used the new HTML5 data-attribute to display comment gravatars on larger screens only. What this means is that on larger posts, mobile sites don't have a ton of requests for comment images. This makes load time incredibly fast! If you'd like to change it back, just replace it with the regular wordpress gravatar call:
          echo get_avatar($comment,$size='32',$default='<path_to_url>' );
        */
        ?>
        <?php // custom gravatar call ?>
        <?php
          // create variable
          $bgauthemail = get_comment_author_email();
        ?>
        <img data-gravatar="http://www.gravatar.com/avatar/<?php echo md5( $bgauthemail ); ?>?s=40" class="load-gravatar avatar avatar-48 photo" height="40" width="40" src="<?php echo get_template_directory_uri(); ?>/library/images/nothing.gif" />
        <?php // end custom gravatar call ?>
        <?php printf(__( '<cite class="fn">%1$s</cite> %2$s', 'bonestheme' ), get_comment_author_link(), edit_comment_link(__( '(Edit)', 'bonestheme' ),'  ','') ) ?>
        <time datetime="<?php echo comment_time('Y-m-j'); ?>"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php comment_time(__( 'F jS, Y', 'bonestheme' )); ?> </a></time>

      </header>
      <?php if ($comment->comment_approved == '0') : ?>
        <div class="alert alert-info">
          <p><?php _e( 'Your comment is awaiting moderation.', 'bonestheme' ) ?></p>
        </div>
      <?php endif; ?>
      <section class="comment_content cf">
        <?php comment_text() ?>
      </section>
      <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
    </article>
  <?php // </li> is added by WordPress automatically ?>
<?php
} // don't remove this bracket!

function testimonial_slider() {
   $args = array( 'post_type' => 'testimonial', 'posts_per_page' => 5 );
   $loop = new WP_Query( $args );
   echo '<div class="testimonial-slider"><div class="row column"><h3><a href="/testimonials/">What our clients say...</a></h3><div class="testimonial-slick">';
   while ( $loop->have_posts() ) : $loop->the_post();
     $name = get_post_meta( get_the_ID(), 'testimonial_name', true );
     $desc = get_post_meta( get_the_ID(), 'testimonial_desc', true ); ?>

     <article id="post-<?php the_ID(); ?>" <?php post_class( 'single-testimonial' ); ?> role="article"> 

       <section class="entry-content cf">

           <div class="testimonial-content"><?php the_excerpt(); ?></div>

          <div class="testimonial-person">
            <div class="testimonial-name"><?php echo esc_html( $name ); ?></div>
            <div class="testimonial-desc"><?php echo esc_html( $desc ); ?></div>
          </div>

       </section>

     </article>

  <?php endwhile;
   echo '</div></div></div>';
}
add_shortcode( 'testimonial-slider', 'testimonial_slider' );

function contact_form() {
  return '
  <form method="POST" name="contact_form" id="contact_form" requestType="import">

    <div class="row">
      <div class="medium-6 column">
        <input type="text" name="firstName" placeholder="First Name*" required>
      </div>
      <div class="medium-6 column">
        <input type="text" name="lastName" placeholder="Last Name*" required>
      </div>
    </div>

    <div class="row">
      <div class="medium-6 column">
        <input type="email" name="email" placeholder="Email Address*" required>
      </div>
      <div class="medium-6 column">
        <input type="tel" name="tel" placeholder="Phone Number" required>
      </div>
    </div>

    <select name="interest">
      <option value="Brand Strategy">Brand Strategy</option>
      <option value="TV">TV</option>
      <option value="Press">Press</option>
      <option value="Direct Marketing">Direct Marketing</option>
      <option value="POS">POS</option>
      <option value="Signage">Signage</option>
      <option value="Digital">Digital</option>
      <option value="Radio">Radio</option>
      <option value="Print">Print</option>
      <option value="Other">Other</option>
    </select>

    <textarea name="message" placeholder="This is how you can help me..."></textarea>
  
    <div class="optin-holder">
      <label><input type="checkbox" name="optIn"> I want to be kept up to date with all the latest news and events from Clear Marketing</label>
    </div>

      <small>We will only contact you in relation to latest news & updates that we think will be of interest to you. </small><br>
      <small>We will not disclose your information to any third party and you can unsubscribe from our database at any time.</small><br>
    <small>Mandatory Fields*</small>

    <div class="input-holder">
      <input type="submit" value="Submit" class="btn-primary">
    </div>
    
  </form>';
}
add_shortcode( 'form-contact', 'contact_form' );

?>