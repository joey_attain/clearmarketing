<?php

flush_rewrite_rules();

function homebanners() { 

	register_post_type( 'homebanner',
		
		array( 'labels' => array(
			'name' => __( 'Home Banners', 'bonestheme' ),
			'singular_name' => __( 'Home Banner', 'bonestheme' ),
			'all_items' => __( 'All Home Banners', 'bonestheme' ),
			'add_new' => __( 'Add New', 'bonestheme' ),
			'add_new_item' => __( 'Add New Home Banner', 'bonestheme' ),
			'edit' => __( 'Edit', 'bonestheme' ),
			'edit_item' => __( 'Edit Home Banners', 'bonestheme' ),
			'new_item' => __( 'New Home Banner', 'bonestheme' ),
			'search_items' => __( 'Search Home Banners', 'bonestheme' ),
			'not_found' =>  __( 'Nothing found in the Database.', 'bonestheme' ),
			'not_found_in_trash' => __( 'Nothing found in Trash', 'bonestheme' ),
			'parent_item_colon' => ''),
			'description' => __( '', 'bonestheme' ),
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => true,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8,
			'capability_type' => 'post',
			'hierarchical' => false,
			'supports' => array( 'title')
		)
	);	
	
}

add_action( 'init', 'homebanners');

?>
