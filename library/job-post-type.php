<?php

flush_rewrite_rules();

function job() { 

	register_post_type( 'job',
		
		array( 'labels' => array(
			'name' => __( 'Join Us', 'bonestheme' ),
			'singular_name' => __( 'Job', 'bonestheme' ),
			'all_items' => __( 'All Jobs', 'bonestheme' ),
			'add_new' => __( 'Add New', 'bonestheme' ),
			'add_new_item' => __( 'Add New Job', 'bonestheme' ),
			'edit' => __( 'Edit', 'bonestheme' ),
			'edit_item' => __( 'Edit Job', 'bonestheme' ),
			'new_item' => __( 'New Job', 'bonestheme' ),
			'search_items' => __( 'Search Jobs', 'bonestheme' ),
			'not_found' =>  __( 'Nothing found in the Database.', 'bonestheme' ),
			'not_found_in_trash' => __( 'Nothing found in Trash', 'bonestheme' ),
			'parent_item_colon' => ''),
			'description' => __( '', 'bonestheme' ),
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => true,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8,
			'rewrite'	=> array( 'slug' => 'job', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'join-us', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			'supports' => array( 'title', 'editor', 'thumbnail', 'revisions')
		)
	);	
	
}

add_action( 'init', 'job');


?>
