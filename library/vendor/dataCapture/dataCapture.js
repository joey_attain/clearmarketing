(function( $ ) {

   $.fn.dataCapture = function( action ) {

      if ( action === 'get_id' ) {
         var refId = "";
         var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
         var timestamp = $.now();

         //append random string
         for (var i = 0; i < 5; i++)
         refId += possible.charAt(Math.floor(Math.random() * possible.length));

         //append timestamp with separator
         refId += '-'+timestamp;

         return refId;
      }

      if ( action === 'get_date' ) {
         var date = new Date($.now()); // returns date and time
         return date;
      }

      if ( action === 'get_url' ) {
         var url = window.location.href; // returns full URL
         return url;
      }

      if ( action === 'get_userAgent' ) {
         var userAgent = navigator.userAgent; // returns user agent
         return userAgent;
      }

      //inject forms
      this.each(function(index) {
         $('input[name="ref_id"]').val( $.fn.dataCapture('get_id') );
         $('input[name="date"]').val( $.fn.dataCapture('get_date') );
         $('input[name="url"]').val( $.fn.dataCapture('get_url') );
         $('input[name="userAgent"]').val( $.fn.dataCapture('get_userAgent') );
      });

   };

})( jQuery );