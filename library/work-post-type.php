<?php

flush_rewrite_rules();

function work() { 

	register_post_type( 'work',
		
		array( 'labels' => array(
			'name' => __( 'Work', 'bonestheme' ),
			'singular_name' => __( 'Work', 'bonestheme' ),
			'all_items' => __( 'All Work', 'bonestheme' ),
			'add_new' => __( 'Add New', 'bonestheme' ),
			'add_new_item' => __( 'Add New Work', 'bonestheme' ),
			'edit' => __( 'Edit', 'bonestheme' ),
			'edit_item' => __( 'Edit Work', 'bonestheme' ),
			'new_item' => __( 'New Work', 'bonestheme' ),
			'search_items' => __( 'Search Work', 'bonestheme' ),
			'not_found' =>  __( 'Nothing found in the Database.', 'bonestheme' ),
			'not_found_in_trash' => __( 'Nothing found in Trash', 'bonestheme' ),
			'parent_item_colon' => ''),
			'description' => __( '', 'bonestheme' ),
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8,
			'rewrite'	=> array( 'slug' => 'work', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'our-work', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			'supports' => array( 'title', 'editor', 'thumbnail', 'revisions', 'excerpt')
		)
	);	

	register_taxonomy_for_object_type( 'category', 'work_cat' );
	
}

add_action( 'init', 'work');

// now let's add custom categories (these act like categories)
register_taxonomy( 'work_cat', 
	array('event'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
	array('hierarchical' => true,     /* if this is true, it acts like categories */
		'labels' => array(
			'name' => __( 'Categories', 'bonestheme' ), /* name of the custom taxonomy */
			'singular_name' => __( 'Category', 'bonestheme' ), /* single taxonomy name */
			'search_items' =>  __( 'Search Categories', 'bonestheme' ), /* search title for taxomony */
			'all_items' => __( 'All Categories', 'bonestheme' ), /* all title for taxonomies */
			'parent_item' => __( 'Parent Category', 'bonestheme' ), /* parent title for taxonomy */
			'parent_item_colon' => __( 'Parent Category:', 'bonestheme' ), /* parent taxonomy title */
			'edit_item' => __( 'Edit Category', 'bonestheme' ), /* edit custom taxonomy title */
			'update_item' => __( 'Update Category', 'bonestheme' ), /* update title for taxonomy */
			'add_new_item' => __( 'Add New Category', 'bonestheme' ), /* add new title for taxonomy */
			'new_item_name' => __( 'New Category Name', 'bonestheme' ) /* name title for taxonomy */
		),
		'show_admin_column' => true, 
		'show_ui' => true,
		'query_var' => true
	)
);

?>
