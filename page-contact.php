<?php
/*
 Template Name: Contact Page
*/
?>

<?php 
$titleBanner_heading = get_post_meta( get_the_ID(), 'titleBanner_heading', true );
$titleBanner_heading_bold = get_post_meta( get_the_ID(), 'titleBanner_heading_bold', true );
$titleBanner_image = get_post_meta( get_the_ID(), 'titleBanner_image', true );
$titleBanner_color = get_post_meta( get_the_ID(), 'titleBanner_color', true );
?>

<?php get_header(); ?>

	<?php if ( get_post_meta( get_the_ID(), 'homebanner_checkbox', 1 ) ) :

		get_template_part( 'templates/entry', 'homebanner' );
		 
	endif; ?>

	<?php if ( get_post_meta( get_the_ID(), 'titleBanner_checkbox', 1 ) ) : ?>
		<div class="page-banner" style="background-image: url(<?php echo esc_html($titleBanner_image) ?>); background-color: <?php echo esc_html($titleBanner_color) ?>">
		    <div class="breadcrumbs-bar">
			    <div class="column row">
				    <?php
					    if ( function_exists('yoast_breadcrumb') ) {
					    yoast_breadcrumb('
					    <p id="breadcrumbs">','</p>
					    ');
				    }
			    ?>
			    </div>
		    </div>
		    <div class="column row">
		        <h1 class="page-banner-heading" makeBold="<?php echo esc_html($titleBanner_heading_bold) ?>"><?php echo esc_html($titleBanner_heading) ?></h1>
		    </div>
		</div>
	<?php endif; ?>

	<div id="content">

		<div class="row column">
			<div class="gmap-info-holder manchester">
				<div class="gmap-gmap">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d64004.946262283825!2d-2.2820161764368647!3d53.404129831046276!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487bb26627c12289%3A0x86e8dc99dd9aba95!2sPalatine+Rd%2C+Manchester+M20+3YA!5e0!3m2!1sen!2suk!4v1509725827812" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
				<div class="gmap-info" style="background-color: #20ace0;">
					<h3><i class="fa fa-map-marker" aria-hidden="true"></i><span class="sr-only">Address</span><strong>Clear Marketing Manchester</strong></h3>
					<p class="addr"><?php echo do_shortcode('[address-manchester]')?></p>
					<p><i class="fa fa-phone" aria-hidden="true"></i><span class="sr-only">Telephone</span><?php echo do_shortcode('[telephone-manchester]')?></p>
					<p><i class="fa fa-envelope" aria-hidden="true"></i><span class="sr-only">Contact at</span>
					<a href="mailto:<?php echo do_shortcode('[email-gemma]')?>"><?php echo do_shortcode('[email-gemma]')?></a> <br>
					<!-- <a class="email-not-top" href="mailto:<?php echo do_shortcode('[email-elaine]')?>"><?php echo do_shortcode('[email-elaine]')?></a> -->
					</p>

					<a href="#contact_form" class="btn-black">Contact Us</a>

				</div>
			</div>

			<div class="gmap-info-holder glasgow">
				<div class="gmap-info" style="background-color: #373d43;">
					<h3><i class="fa fa-map-marker" aria-hidden="true"></i><span class="sr-only">Address</span><strong>Clear Marketing Glasgow</strong></h3>
					<p class="addr"><?php echo do_shortcode('[address-glasgow]')?></p>
					<p><i class="fa fa-phone" aria-hidden="true"></i><span class="sr-only">Telephone</span><?php echo do_shortcode('[telephone-glasgow]')?></p>
					<p><i class="fa fa-envelope" aria-hidden="true"></i><span class="sr-only">Contact at</span>
					<a href="mailto:<?php echo do_shortcode('[email-matthew]')?>"><?php echo do_shortcode('[email-matthew]')?></a></p>

					<a href="#contact_form" class="btn-primary">Contact Us</a>

				</div>
				<div class="gmap-gmap">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2238.844068071181!2d-4.278106484065095!3d55.86537178058366!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4888442bc7a81607%3A0x55e27a8c0c3cbb0a!2s116+Elderslie+St%2C+Glasgow+G3!5e0!3m2!1sen!2suk!4v1512470317848" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
			</div>
		</div>

		<div id="inner-content" class="row column">

				<main id="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class( '' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

						<section class="entry-content" itemprop="articleBody">

							<?php the_content(); ?>

						</section> <?php // end article section ?>

					</article>

					<?php endwhile; endif; ?>

				</main>

		</div>

	</div>
<?php echo do_shortcode('[testimonial-slider]'); ?>
<?php get_footer(); ?>
