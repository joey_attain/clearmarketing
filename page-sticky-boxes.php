<?php
/*
 Template Name: Sticky Boxes
*/
$banner_heading = get_post_meta( get_the_ID(), 'banner_heading', true );
$banner_image = get_post_meta( get_the_ID(), 'banner_image', true );
?>

<?php get_header(); ?>

	<?php if ( get_post_meta( get_the_ID(), 'homebanner_checkbox', 1 ) ) :

		get_template_part( 'templates/entry', 'homebanner' );
		 
	endif; ?>

	<?php if ( $banner_heading != '' ) : ?>
		<div class="page-banner" style="background-image: url(<?php echo esc_html($banner_image) ?>)">
		    <div class="column row">
		        <h1><?php echo esc_html($banner_heading) ?></h1>
		    </div>
		    <div class="breadcrumbs-bar">
			    <div class="column row">
				    <?php
					    if ( function_exists('yoast_breadcrumb') ) {
					    yoast_breadcrumb('
					    <p id="breadcrumbs">','</p>
					    ');
				    }
			    ?>
			    </div>
		    </div>
		</div>
	<?php endif; ?>

	<div id="sticky-boxes-inner-content" class="row column pull-up-for-video">

			<main id="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class( '' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

					<section itemprop="articleBody">
						<?php the_content(); ?>
					</section> <?php // end article section ?>

				</article>

				<?php endwhile; endif; ?>

			</main>

	</div>
<?php echo do_shortcode('[testimonial-slider]'); ?>
<?php get_footer(); ?>
