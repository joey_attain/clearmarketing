<?php
/*
 Template Name: Testimonial Archive
*/
?>


<?php get_header(); ?>

<div class="page-banner" style="background-image: url(/wp-content/uploads/testimonials-banner-new.jpg);">
    <div class="breadcrumbs-bar">
	    <div class="column row">
		    <?php
			    if ( function_exists('yoast_breadcrumb') ) {
			    yoast_breadcrumb('
			    <p id="breadcrumbs">','</p>
			    ');
		    }
	    ?>
	    </div>
    </div>
    <div class="column row">
        <h1 class="page-banner-heading">Read what other people have to say about us...</h1>
    </div>
</div>

	<div id="content">

		<div id="inner-content" class="row column">

				<main id="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

					<div class="flexy-row">

						<?php
							$args = array(
								'post_type' => array('testimonial'),
								'posts_per_page' => 6,
		    					'paged'          => get_query_var( 'paged' )
							);

							$wp_query = new WP_Query( $args );

			            	if ($wp_query->have_posts()) {
			            		while ($wp_query->have_posts()) { 
			            		$wp_query->the_post();

			            		$name = get_post_meta( get_the_ID(), 'testimonial_name', true );
			            		$desc = get_post_meta( get_the_ID(), 'testimonial_desc', true );
								$thumbnail = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
								$color_scheme_1 = get_post_meta( get_the_ID(), 'testimonial_color_scheme_1', true ); 
								$color_scheme_2 = get_post_meta( get_the_ID(), 'testimonial_color_scheme_2', true ); 
								?>

								<div class="single-testimonial-post stp-<?php the_ID(); ?>" style="background-color:<?php echo $color_scheme_1; ?>;border:solid 5px <?php echo $color_scheme_1; ?>;">

									<article id="post-<?php the_ID(); ?>" <?php post_class( 'single testimonial' ); ?> role="article">

										<div class="testimonial-card">

											<?php if ( has_post_thumbnail() ) { ?> 
												<?php the_post_thumbnail('full');
											 } ?>

											<section class="entry-content cf">

											    <div class="testimonial-content"><?php the_content(); ?></div>

											   <div class="testimonial-person">
											     <div class="testimonial-name" style="color:<?php echo $color_scheme_2; ?>;"><?php echo esc_html( $name ); ?></div>
											     <div class="testimonial-desc"><?php echo esc_html( $desc ); ?></div>
											   </div>

											</section>
											
										</div>

									</article>

								</div>

								<style type="text/css">
									.single-testimonial-post.stp-<?php the_ID(); ?>::before,
									.single-testimonial-post.stp-<?php the_ID(); ?>::after {
										background-color: <?php echo $color_scheme_2; ?>
									}
								</style>

						<?php  
								}
								bones_page_navi();
							}
						?>

						<?php wp_reset_postdata(); ?>

				</main>

		</div>

	</div>

<?php get_footer(); ?>
