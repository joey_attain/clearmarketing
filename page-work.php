<?php
/*
 Template Name: Work Archive
*/
?>


<?php get_header(); ?>

<div class="page-banner" style="background-image: url(/wp-content/uploads/our-work-banner.jpg);">
    <div class="breadcrumbs-bar">
	    <div class="column row">
		    <?php
			    if ( function_exists('yoast_breadcrumb') ) {
			    yoast_breadcrumb('
			    <p id="breadcrumbs">','</p>
			    ');
		    }
	    ?>
	    </div>
    </div>
    <div class="column row">
        <h1 class="page-banner-heading" makeBold="our work">View examples of our work</h1>
    </div>
</div>

	<div id="content">

		<div id="inner-content" class="row column">

				<main id="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

					<div class="row">

						<?php
							$args = array(
								'post_type' => array('work'),
								'posts_per_page' => 6,
		    					'paged'          => get_query_var( 'paged' )
							);

							$wp_query = new WP_Query( $args );

			            	if ($wp_query->have_posts()) {
			                	$i = 1;
			            		while ($wp_query->have_posts()) { 
			            		$wp_query->the_post();

			            		if ($i === 1) {
			            		    $grid_size = "12";
			            		} 
			            		else if (($wp_query->current_post +1) == ($wp_query->post_count)) {
			            			$grid_size = "12";
			            		}
			            		else {
			            			$grid_size = '6';
			            		}

			            		$work_company_name = get_post_meta( get_the_ID(), 'work_company_name', true );
								$thumbnail = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>

								<div class="medium-<?php echo $grid_size; ?> column">

									<article id="post-<?php the_ID(); ?>" <?php post_class( 'single work' ); ?> role="article">

										<div class="work-card" style="background-image: url('<?=($thumbnail) ? $thumbnail : "" ?>');">

											<div class="cta-strip">
											    <div class="cta-strip-left">
											        <h3><?php echo esc_html( $work_company_name ); ?></h3>
											        <p><?php the_title(); ?></p>
											    </div>
											    <div class="cta-strip-right">
											        <a href="<?php the_permalink() ?>" class="btn-hollow-white-alt">View Project</a>
											    </div>
											</div>
											
										</div>

									</article>

								</div>

						<?php  
							$i++;
								}
								bones_page_navi();
							}
						?>

						<?php wp_reset_postdata(); ?>

					</div>

				</main>

		</div>

	</div>

<?php get_footer(); ?>
