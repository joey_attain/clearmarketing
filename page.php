<?php 
$titleBanner_heading = get_post_meta( get_the_ID(), 'titleBanner_heading', true );
$titleBanner_heading_bold = get_post_meta( get_the_ID(), 'titleBanner_heading_bold', true );
$titleBanner_image = get_post_meta( get_the_ID(), 'titleBanner_image', true );
$titleBanner_color = get_post_meta( get_the_ID(), 'titleBanner_color', true );
?>

<?php get_header(); ?>

	<?php if ( get_post_meta( get_the_ID(), 'homebanner_checkbox', 1 ) ) :

		get_template_part( 'templates/entry', 'homebanner' );
		 
	endif; ?>

	<?php if ( get_post_meta( get_the_ID(), 'titleBanner_checkbox', 1 ) ) : ?>
		<div class="page-banner" style="background-image: url(<?php echo esc_html($titleBanner_image) ?>); background-color: <?php echo esc_html($titleBanner_color) ?>">
		    <div class="breadcrumbs-bar">
			    <div class="column row">
				    <?php
					    if ( function_exists('yoast_breadcrumb') ) {
					    yoast_breadcrumb('
					    <p id="breadcrumbs">','</p>
					    ');
				    }
			    ?>
			    </div>
		    </div>
		    <div class="column row">
		        <h1 class="page-banner-heading" makeBold="<?php echo esc_html($titleBanner_heading_bold) ?>"><?php echo esc_html($titleBanner_heading) ?></h1>
		    </div>
		</div>
	<?php endif; ?>

	<div id="content">

		<div id="inner-content" class="row column">

				<main id="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class( '' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

						<section class="entry-content" itemprop="articleBody">
							<?php the_content(); ?>
						</section> <?php // end article section ?>

					</article>

					<?php endwhile; endif; ?>

				</main>

		</div>

	</div>
<?php echo do_shortcode('[testimonial-slider]'); ?>
<?php get_footer(); ?>
