<?php get_header(); $post_type = isset($_GET['post_type']) ? $_GET['post_type'] : 'search'; ?>

<div class="page-banner" style="background-image: url(/wp-content/uploads/our-blog-banner.jpg);">
    <div class="breadcrumbs-bar">
	    <div class="column row">
		    <?php
			    if ( function_exists('yoast_breadcrumb') ) {
			    yoast_breadcrumb('
			    <p id="breadcrumbs">','</p>
			    ');
		    }
	    ?>
	    </div>
    </div>
    <div class="column row">
        <h1 class="page-banner-heading"><span><?php _e( 'Search Results for:', 'bonestheme' ); ?></span> <?php echo esc_attr(get_search_query()); ?></h1>
    </div>
</div>

	<div id="content">

		<div id="inner-content" class="row">

			<main id="main" class="medium-8 column">

					<div class="row">

					<?php

	                $postcount = 1; // set 0 with featured sticky
	            	?>

					<?php if ($wp_query->have_posts()) {
						 while ($wp_query->have_posts()) { $wp_query->the_post();

							if($postcount % 2 != 0) { echo '</div>'; }
						    $postcount++;
						    if($postcount % 2 == 0) { echo '<div class="row">'; }
						 
							get_template_part('templates/entry', 'post');

						} ?>

						</div>

						<?php bones_page_navi(); ?>

					<?php } ?>

				</main>

					<?php get_sidebar(); ?>

			</div>

	</div>

<?php get_footer(); ?>
