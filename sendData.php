<?php

/*
*  @retrieve results from ajax request for contact form
*/
if (isset($_POST['dataCapture'])) {
    global $conn;

    //decode data from json format
    $data = json_decode($_POST['dataCapture'],true);

    // Only process POST requests.
    if ($_SERVER["REQUEST_METHOD"] == "POST") {

        //set vars from posted data
        $ref_id = isset($data['refId']) ? $data['refId'] : '';
        //if any required field is empty print message.
        //(this will be used when browsers don't support the javascript/html5 solution for required fields)
        if (empty($first_name) OR empty($last_name) OR empty($company) OR empty($email)) {
            echo json_encode('*Warning: You must fill out the required fields to send.');
            exit();
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            echo json_encode('*Warning: Please enter a valid email.');
            exit();
        }

        // $sql = "INSERT INTO cyberEssentialsLanding (ref_id,first_name,last_name,company,email,interested_in,date_submitted,optIn,userAgent,url) VALUES ('$ref_id','$first_name','$last_name','$company','$email','$interested_in',NOW(),'$optIn','$userAgent','$url')";
        // $dataCapture = mysqli_query($conn, $sql);
        // mysqli_close($conn);

    } else {
        exit();
    }

    //return array back to ajax request
    echo json_encode($sql);
    exit();
}
?>