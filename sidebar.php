<div id="sidebar1" class="sidebar medium-4 column end" role="complementary">

	<div class="sidebar-heading">
		<h2>Filter</h2>
		<hr>
	</div>

	<div class="sidebar-module">
		<form action="/">
		    <input type="hidden" name="post_type" value="post"/>

		    <input type="text" placeholder="Search" name="s" value="" id="keyword" autocomplete="off">

		    <input type="submit" value="Go" class="btn-hollow-gray" style="display:none;">
		</form>
	</div>

	<div class="sidebar-module trinidad">
		<h3 class="sidebar-module-heading">Archives</h3>
		<ul class="sidebar-menu menu">
			<?php 
			$args = array(
			    'type'            => 'monthly',
			    'limit'           => '',
			    'format'          => 'html', 
			    'before'          => '',
			    'after'           => '',
			    'show_post_count' => false,
			    'echo'            => 1,
			    'order'           => 'DESC'
			);
			wp_get_archives( $args );
			?>
		</ul>
	</div>

	<div class="sidebar-module trinidad">
		<h3 class="sidebar-module-heading">Categories</h3>
		<ul class="sidebar-menu menu">
			<?php
			wp_list_categories( array(
			    'taxonomy'		=> 'category',
			    'hide_empty'	=> true,
			    'orderby'		=> 'name',
			    'title_li'      => false,
			) );
			?>
		</ul>
	</div>

</div>
