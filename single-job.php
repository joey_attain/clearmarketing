
<?php
$job_salary = get_post_meta( get_the_ID(), 'job_salary', true );
$job_loc = get_post_meta( get_the_ID(), 'job_loc', true );
$job_pos = get_post_meta( get_the_ID(), 'job_pos', true );
$job_colorScheme = get_post_meta( get_the_ID(), 'job_colorScheme', true );
$thumbnail = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
?>

<?php get_header(); ?>

<div class="page-banner" style="background-image: url(/wp-content/uploads/join-us-banner.jpg);">
    <div class="breadcrumbs-bar">
	    <div class="column row">
		    <?php
			    if ( function_exists('yoast_breadcrumb') ) {
			    yoast_breadcrumb('
			    <p id="breadcrumbs">','</p>
			    ');
		    }
	    ?>
	    </div>
    </div>
    <div class="column row">
        <h1 class="page-banner-heading" makeBold="Join us">Be a part of something great! Join us</h1>
    </div>
</div>

	<div>

		<div class="job-header" style="background-color: <?php echo esc_html($job_colorScheme); ?>">
			<h2 class="single-title custom-post-type-title"><?php the_title(); ?></h2>
		</div>

		<div class="job-details">
			<div>
				<i class="fa fa-gbp" aria-hidden="true" style="background-color:<?php echo esc_html($job_colorScheme); ?>;"></i><span class="sr-only">Salary:</span>
				<span><?php echo esc_html($job_salary); ?></span>
			</div>
			<div>
				<i class="fa fa-map-marker" aria-hidden="true" style="background-color:<?php echo esc_html($job_colorScheme); ?>;"></i><span class="sr-only">Location:</span>
				<span><?php echo esc_html($job_loc); ?></span>
			</div>
			<div>
				<i class="fa fa-clock-o" aria-hidden="true" style="background-color:<?php echo esc_html($job_colorScheme); ?>;"></i><span class="sr-only">Position:</span>
				<span><?php echo esc_html($job_pos); ?></span>
			</div>
		</div>

		<div id="inner-content" class="row column">

			<main id="main"  itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<div class="entry-content">

						<div style="text-align: center;"><a href="/contact-us/" class="btn-dynamic"  style="background-color:<?php echo esc_html($job_colorScheme); ?>;border-color:<?php echo esc_html($job_colorScheme); ?>;">Apply for job</a></div>
						<br><br>

						<?php
							the_content();
						?>

						<div style="text-align: center;"><a href="/contact-us/" class="btn-dynamic"  style="background-color:<?php echo esc_html($job_colorScheme); ?>;border-color:<?php echo esc_html($job_colorScheme); ?>;">Apply for job</a></div>

						<div class="share-col">
							<h4>Share this job</h4>
							<ul class="share-buttons">
							  <li><a id="facebook" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fwww.clearmarketing.com%2F&t=" title="Share on Facebook" target="_blank" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(document.URL) + '&t=' + encodeURIComponent(document.URL)); return false;"><i class="fa fa-facebook-official" aria-hidden="true"></i><span class="sr-only">Facebook</span></a></li>
							  <li><a id="twitter" href="https://twitter.com/intent/tweet?source=http%3A%2F%2Fwww.clearmarketing.com%2F&text=:%20http%3A%2F%2Fwww.clearmarketing.com%2F" target="_blank" title="Tweet" onclick="window.open('https://twitter.com/intent/tweet?text=' + encodeURIComponent(document.title) + ' @clearmarketing '  + encodeURIComponent(document.URL)); return false;"><i class="fa fa-twitter" aria-hidden="true"></i><span class="sr-only">Twitter</span></a></li>
							  <li><a id="linkedin" href="http://www.linkedin.com/shareArticle?mini=true&url=http%3A%2F%2Fwww.clearmarketing.com%2F&title=&summary=&source=http%3A%2F%2Fwww.clearmarketing.com%2F" target="_blank" title="Share on LinkedIn" onclick="window.open('http://www.linkedin.com/shareArticle?mini=true&url=' + encodeURIComponent(document.URL) + '&title=' +  encodeURIComponent(document.title)); return false;"><i class="fa fa-linkedin" aria-hidden="true"></i><span class="sr-only">Linkedin</span></a></li>
							  <li><a id="email" href="mailto:?subject=&body=:%20http%3A%2F%2Fwww.clearmarketing.com%2F" target="_blank" title="Send email" onclick="window.open('mailto:?subject=' + encodeURIComponent(document.title) + '&body=' +  encodeURIComponent(document.URL)); return false;"><i class="fa fa-envelope" aria-hidden="true"></i><span class="sr-only">Email</span></a></li>
							</ul>
						</div>
					</div> <!-- end article section -->



				<?php endwhile; ?>

				<?php else : ?>

					<article id="post-not-found" class="hentry">
							<header class="article-header">
								<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
							</header>
							<section class="entry-content">
								<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
							</section>
							<footer class="article-footer">
									<p><?php _e( 'This is the error message in the single.php template.', 'bonestheme' ); ?></p>
							</footer>
					</article>

				<?php endif; ?>

			</main>

		</div>

	</div>
<?php echo do_shortcode('[testimonial-slider]'); ?>
<?php get_footer(); ?>
