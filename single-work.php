<?php 
$titleBanner_heading = get_post_meta( get_the_ID(), 'titleBanner_heading', true );
$titleBanner_heading_bold = get_post_meta( get_the_ID(), 'titleBanner_heading_bold', true );
$titleBanner_image = get_post_meta( get_the_ID(), 'titleBanner_image', true );
$titleBanner_color = get_post_meta( get_the_ID(), 'titleBanner_color', true );
?>


<?php get_header(); ?>

<?php if ( get_post_meta( get_the_ID(), 'titleBanner_checkbox', 1 ) ) { ?>

		<div class="page-banner" style="background-image: url(<?php echo esc_html($titleBanner_image) ?>); background-color: <?php echo esc_html($titleBanner_color) ?>">
		    <div class="breadcrumbs-bar">
			    <div class="column row">
				    <?php
					    if ( function_exists('yoast_breadcrumb') ) {
					    yoast_breadcrumb('
					    <p id="breadcrumbs">','</p>
					    ');
				    }
			    ?>
			    </div>
		    </div>
		    <div class="column row">
		        <h1 class="page-banner-heading" makeBold="<?php echo esc_html($titleBanner_heading_bold) ?>"><?php echo esc_html($titleBanner_heading) ?></h1>
		    </div>
		</div>

<?php } else { ?>

	<div class="page-banner" style="background-image: url(/wp-content/uploads/work_header_banner-1.jpg);">
	    <div class="breadcrumbs-bar">
		    <div class="column row">
			    <?php
				    if ( function_exists('yoast_breadcrumb') ) {
				    yoast_breadcrumb('
				    <p id="breadcrumbs">','</p>
				    ');
			    }
		    ?>
		    </div>
	    </div>
	    <div class="column row">
	        <h1 class="page-banner-heading" makeBold="our work">View examples of our work</h1>
	    </div>
	</div>

<?php } ?>

	<div>

		<div id="inner-content" class="column row">

				<main id="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article">

						<section class="entry-content" style="padding-top: 0;">
							<?php the_content(); ?>
						</section>

					</article>

					<?php endwhile; ?>

					<?php else : ?>

							<article id="post-not-found" class="hentry">
								<header class="article-header">
									<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
								</header>
							</article>

					<?php endif; ?>

				</main>

				<div class="post-navi">
					<div class="row column">
						<div class="medium-7 column">
							
						</div>
						<div class="medium-5 column">
							<?php next_post_link( '%link', '<span class="btn-primary"><i class="fa fa-chevron-left" aria-hidden="true"></i> &nbsp; Previous  </span>', FALSE ); ?>
							<?php previous_post_link( '%link', '<span class="btn-primary">Next &nbsp; <i class="fa fa-chevron-right" aria-hidden="true"></i></span>', FALSE ); ?>
						</div>
					</div>
				</div>

				<br>

		</div>

	</div>

<?php get_footer(); ?>
