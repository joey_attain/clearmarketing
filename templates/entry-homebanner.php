<?php
 $args = array( 'post_type' => 'homebanner', 'posts_per_page' => 10 );
 $loop = new WP_Query( $args );
 echo '<div class="homebanner-slider"><div class="homebanner-slick">';
 while ( $loop->have_posts() ) : $loop->the_post();
     // Grab the metadata from the database
     $homebanner_heading = get_post_meta( get_the_ID(), 'homebanner_heading', true );
     $homebanner_image = get_post_meta( get_the_ID(), 'homebanner_image', true );
     $homebanner_button_text = get_post_meta( get_the_ID(), 'homebanner_button_text', true );
     $homebanner_button_url = get_post_meta( get_the_ID(), 'homebanner_button_url', true );
     $homebanner_youtube_video = get_post_meta( get_the_ID(), 'homebanner_youtube_video', true );
     $homebanner_full_width_video_image = get_post_meta( get_the_ID(), 'homebanner_full_width_video_image', true );
     $homebanner_full_width_video_mp4 = get_post_meta( get_the_ID(), 'homebanner_full_width_video_mp4', true );
     $homebanner_full_width_video_webm = get_post_meta( get_the_ID(), 'homebanner_full_width_video_webm', true );
     $banner_type = '';
     if($homebanner_youtube_video != '') { $banner_type = 'video-banner'; }
     if($homebanner_full_width_video_mp4 != '') { $banner_type = 'full-video-banner'; }
     ?>

   <div id="post-<?php the_ID(); ?>" <?php post_class( 'single-homebanner '. $banner_type ); ?> role="article" style="background-image: url(<?php echo esc_html($homebanner_image);?>)"> 

    <?php if($homebanner_full_width_video_mp4 != '') { ?>
    <section class="video-section">
        <video poster="<?php echo $homebanner_full_width_video_image; ?>" id="bgvid" playsinline autoplay muted loop preload="none">
          <!-- WCAG general accessibility recommendation is that media such as background video play through only once. Loop turned on for the purposes of illustration; if removed, the end of the video will fade in the same way created by pressing the "Pause" button  -->
        <source src="<?php echo $homebanner_full_width_video_webm; ?>" type="video/webm">
        <source src="<?php echo $homebanner_full_width_video_mp4; ?>" type="video/mp4">
        </video>
        <div class="content-over-video" height="486px">
            <div class="content-over-video-inner">
                <h2 class="homebanner-name"> <?php echo esc_html( $homebanner_heading ); ?></h2>
                <a href="<?php echo esc_html( $homebanner_button_url ); ?>" class="btn-hollow-white"><?php echo esc_html( $homebanner_button_text ); ?></a>
            </div>
        </div>
    </section>

    <script>
    var promise = document.querySelector('video').play();

    if (promise !== undefined) {
    promise.then(_ => {
        console.log('started');
    }).catch(error => {
        console.log('prevented');
        // Show a "Play" button so that user can start playback.
    });
    }
    </script>

    <?php } else { ?>

     <section class="column row">

     	<?php if($homebanner_youtube_video != '') { ?>
     		<div class="video-responsive">
     		    <iframe width="600" height="350" src="<?php echo $homebanner_youtube_video; ?>" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
     		</div>
     	<?php } else { ?>
         <h2 class="homebanner-name"> <?php echo esc_html( $homebanner_heading ); ?></h2>
         <a href="<?php echo esc_html( $homebanner_button_url ); ?>" class="btn-hollow-white"><?php echo esc_html( $homebanner_button_text ); ?></a>
         <?php } ?>
     </section>

         <script>
        var promise = document.querySelector('video').play();

        if (promise !== undefined) {
        promise.then(_ => {
            console.log('started');
        }).catch(error => {
            console.log('prevented');
            // Show a "Play" button so that user can start playback.
        });
        }
        </script>

     <?php } ?>

   </div>

<?php endwhile;
 echo '</div></div>';