<div class="large-12 column featured-col">

<?php if( get_post_type() == 'event' ) { ?>

<?php
$slug = array_pop(get_the_terms($post->ID, 'event_type'))->slug;
$name = array_pop(get_the_terms($post->ID, 'event_type'))->name;
?>
    
	<article id="post-<?php the_ID(); ?>" <?php post_class( 'post-card featured type-'. $slug ); ?> role="article">

<?php } else { ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class( 'post-card featured' ); ?> role="article">

<?php } ?>

		<div class="row">

			<div class="medium-5 column">
				<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
				<?php if ( has_post_thumbnail() ) { ?> 
					<?php the_post_thumbnail('medium');
				 } else {
				 	if ( get_theme_mod( 'bones_post_placeholder' ) ) {
				 	  echo '<img src="'. get_theme_mod( 'bones_post_placeholder' ) .'" alt="'. get_the_title() .'"/>';
				 	}
				 } ?>

				  <?php if( get_post_type() == 'event' ) { ?>

				 	 <div class="event-type-tag">
				 	 	<a href="/event_type/<?php echo array_pop(get_the_terms($post->ID, 'event_type'))->slug; ?>"><?php echo array_pop(get_the_terms($post->ID, 'event_type'))->name; ?></a>
				 	 </div>
				  
				  <?php } ?>
				 	
				 </a>
			</div>
			
			<div class="medium-7 column">

				<div class="post-content">

					<header class="article-header">

						<h3 class="h2"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>

					</header>

					<section class="entry-content">

						<?php the_excerpt(); ?>

					</section>

					<footer class="article-footer">

<!-- 						<p class="byline vcard">

							<?php
							$auth_fname = get_the_author_meta('first_name');
							$auth_lname = get_the_author_meta('last_name');
							?>

							<span class="post-author"> <a href="<?php echo get_author_posts_url(get_the_author_meta('id')); ?>">  By <?php echo trim( "$auth_fname $auth_lname" ); ?></a></span>

							<span class="post-date"> <?php printf( __( '<time class="updated" datetime="%1$s" itemprop="datePublished">%2$s</time>', 'bonestheme' ), get_the_time( 'Y-m-j' ), get_the_time( __( 'n M Y', 'bonestheme' ) )); ?> </span>

						</p> -->

					</footer>

				</div>
				
			</div>

		</div>

	</article>

</div>