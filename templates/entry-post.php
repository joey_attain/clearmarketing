<div class="medium-6 column <?php if ($wp_the_query->current_post +1 == $wp_the_query->post_count) { echo 'end'; } ?>">

	<article id="post-<?php the_ID(); ?>" <?php post_class( 'post-card' ); ?> role="article">

		<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
			<div class="post-thumb-holder">
				<?php if ( has_post_thumbnail() ) { ?> 
					<?php the_post_thumbnail('large');
				 } else {
				 	if ( get_theme_mod( 'bones_post_placeholder' ) ) {
				 	  echo '<img src="'. get_theme_mod( 'bones_post_placeholder' ) .'" alt="'. get_the_title() .'" class="placeholder-post"/>';
				 	}
				 } ?>

		 	</div>
		 </a>

		 <div class="post-content">

			<header class="article-header">

				<h3><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>

			</header>

			<section class="entry-content">

				<?php the_excerpt(); ?>

			</section>

			<footer class="article-footer">

				<p class="byline vcard">

					<span class="post-author"> <a href="<?php echo get_author_posts_url(get_the_author_meta('id')); ?>">  By <?php echo get_the_author_meta('first_name') ?></a></span>


					<span class="post-date"> <?php printf( __( '<time class="updated" datetime="%1$s" itemprop="datePublished">%2$s</time>', 'bonestheme' ), get_the_time( 'Y-m-j' ), get_the_time( __( 'n M Y', 'bonestheme' ) )); ?> </span>

				</p>

			</footer>

		</div>

	</article>

</div>

