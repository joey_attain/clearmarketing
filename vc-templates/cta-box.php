<?php

// Element Class 
class cta_box extends WPBakeryShortCode {
     
    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'vc_cta_box_mapping' ) );
        add_shortcode( 'vc_cta_box', array( $this, 'vc_cta_box_html' ) );
    }

    // Element Mapping
    public function vc_cta_box_mapping() {
         
        // Stop all if VC is not enabled
            if ( !defined( 'WPB_VC_VERSION' ) ) {
                    return;
            }
                 
            // Map the block with vc_map()
            vc_map( 
          
                array(
                    'name' => __('Grid Link Box', 'text-domain'),
                    'base' => 'vc_cta_box',
                    'description' => __('Show boxed CTA with an image and a button', 'text-domain'), 
                    'category' => __('ATTAIN Elements', 'text-domain'),
                    'params' => array(  

                        array(
                            'type' => 'attach_image',
                            'holder' => 'img',
                            'heading' => __( 'Background Image', 'text-domain' ),
                            'param_name' => 'vc_cta_box_bg_image',
                            'value' => __( '', 'text-domain' ),
                            'admin_label' => false,
                            'weight' => 0,
                        ),

                        array(
                            'type' => 'attach_image',
                            'holder' => 'img',
                            'heading' => __( 'Image', 'text-domain' ),
                            'param_name' => 'vc_cta_box_image',
                            'value' => __( '', 'text-domain' ),
                            'admin_label' => false,
                            'weight' => 0,
                        ),

                        array(
                            'type' => 'textfield',
                            'holder' => 'h3',
                            'heading' => __( 'Heading', 'text-domain' ),
                            'param_name' => 'vc_cta_box_heading',
                            'value' => __( '', 'text-domain' ),
                            'admin_label' => false,
                            'weight' => 0,
                        ),

                        array(
                            'type' => 'textfield',
                            'heading' => __( 'Heading Size', 'text-domain' ),
                            'param_name' => 'vc_cta_box_heading_size',
                            'value' => __( '', 'text-domain' ),
                            'admin_label' => false,
                            'weight' => 0,
                        ),

                        array(
                            'type' => 'vc_link',
                            'heading' => __( 'Link', 'text-domain' ),
                            'param_name' => 'vc_cta_box_link',
                            'value' => __( '', 'text-domain' ),
                            'admin_label' => false,
                            'weight' => 0,
                        ),

                        array(
                            'type' => 'textfield',
                            'holder' => 'p',
                            'heading' => __( 'Link Text', 'text-domain' ),
                            'param_name' => 'vc_cta_box_link_text',
                            'value' => __( '', 'text-domain' ),
                            'admin_label' => false,
                            'weight' => 0,
                        ),

                        array(
                            'type' => 'colorpicker',
                            'holder' => 'p',
                            'heading' => __( 'Box colour', 'text-domain' ),
                            'param_name' => 'vc_cta_box_colorscheme1',
                            'value' => __( '', 'text-domain' ),
                            'admin_label' => false,
                            'weight' => 0,
                        ),

                        array(
                            'type' => 'colorpicker',
                            'holder' => 'p',
                            'heading' => __( 'Content colour', 'text-domain' ),
                            'param_name' => 'vc_cta_box_colorscheme2',
                            'value' => __( '', 'text-domain' ),
                            'admin_label' => false,
                            'weight' => 0,
                        ),
                             
                    )
                )
            );
        
    } 
     
    // Element HTML
    public function vc_cta_box_html( $atts ) {

        extract(
            shortcode_atts(
                array(
                    'vc_cta_box_bg_image'        => '',
                    'vc_cta_box_image'        => '',
                    'vc_cta_box_heading'        => '',
                    'vc_cta_box_heading_size'        => '',
                    'vc_cta_box_link'        => '',
                    'vc_cta_box_link_text'        => 'View More',
                    'vc_cta_box_colorscheme1'        => '',
                    'vc_cta_box_colorscheme2'        => '',
                ), 
                $atts
            )
        );

        $href = vc_build_link($vc_cta_box_link);
        $bg_image_url = wp_get_attachment_image_src( $attachment_id = $vc_cta_box_bg_image, 'full' );
        $image_url = wp_get_attachment_image_src( $attachment_id = $vc_cta_box_image, 'box-icon' );
        $image_alt = get_post_meta( $image->id, '_wp_attachment_image_alt', true);

        $html = '
        <section>
            <div class="cta-box" style="background-color:'. $vc_cta_box_colorscheme1 .';background-image:url('.$bg_image_url[0].');">
                <img src="'. $image_url[0] .'" alt="'. $image_alt .'" title="'. $vc_cta_box_heading .'" >
                <h3 style="color:'. $vc_cta_box_colorscheme2 .'!important;font-size:'. $vc_cta_box_heading_size .'">'. $vc_cta_box_heading .'</h3>';

                if($vc_cta_box_link) {
                $html .= '<a href="'. $href["url"] .'" class="btn btn-hollow-dynamic" style="color:'. $vc_cta_box_colorscheme2 .'; border-color: '.$vc_cta_box_colorscheme2.'">'. $vc_cta_box_link_text .'</a>';
                }

            $html .= '</div>
        </section>
        ';

        return $html;

    } 
     
} // End Element Class
 
// Element Class Init
new cta_box();    
?>