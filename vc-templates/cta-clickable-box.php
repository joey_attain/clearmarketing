<?php

// Element Class 
class cta_clickable_box extends WPBakeryShortCode {
     
    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'vc_cta_clickable_box_mapping' ) );
        add_shortcode( 'vc_cta_clickable_box', array( $this, 'vc_cta_clickable_box_html' ) );
    }

    // Element Mapping
    public function vc_cta_clickable_box_mapping() {
         
        // Stop all if VC is not enabled
            if ( !defined( 'WPB_VC_VERSION' ) ) {
                    return;
            }
                 
            // Map the block with vc_map()
            vc_map( 
          
                array(
                    'name' => __('Grid Button Link Box', 'text-domain'),
                    'base' => 'vc_cta_clickable_box',
                    'description' => __('Show boxed CTA with an image that acts as a button', 'text-domain'), 
                    'category' => __('ATTAIN Elements', 'text-domain'),
                    'params' => array(  

                        array(
                            'type' => 'attach_image',
                            'holder' => 'img',
                            'heading' => __( 'Background Image', 'text-domain' ),
                            'param_name' => 'vc_cta_clickable_box_bg_image',
                            'value' => __( '', 'text-domain' ),
                            'admin_label' => false,
                            'weight' => 0,
                        ),

                        array(
                            'type' => 'attach_image',
                            'holder' => 'img',
                            'heading' => __( 'Image', 'text-domain' ),
                            'param_name' => 'vc_cta_clickable_box_image',
                            'value' => __( '', 'text-domain' ),
                            'admin_label' => false,
                            'weight' => 0,
                        ),

                        array(
                            'type' => 'textfield',
                            'holder' => 'h3',
                            'heading' => __( 'Heading', 'text-domain' ),
                            'param_name' => 'vc_cta_clickable_box_heading',
                            'value' => __( '', 'text-domain' ),
                            'admin_label' => false,
                            'weight' => 0,
                        ),

                        array(
                            'type' => 'textarea',
                            'holder' => 'p',
                            'heading' => __( 'Tooltip content', 'text-domain' ),
                            'param_name' => 'vc_cta_clickable_box_tooltip',
                            'value' => __( '', 'text-domain' ),
                            'admin_label' => false,
                            'weight' => 0,
                        ),

                        array(
                            'type' => 'vc_link',
                            'heading' => __( 'Link', 'text-domain' ),
                            'param_name' => 'vc_cta_clickable_box_link',
                            'value' => __( '', 'text-domain' ),
                            'admin_label' => false,
                            'weight' => 0,
                        ),

                        array(
                            'type' => 'colorpicker',
                            'holder' => 'p',
                            'heading' => __( 'Box colour', 'text-domain' ),
                            'param_name' => 'vc_cta_clickable_box_colorscheme1',
                            'value' => __( '', 'text-domain' ),
                            'admin_label' => false,
                            'weight' => 0,
                        ),
                        array(
                            'type' => 'colorpicker',
                            'holder' => 'p',
                            'heading' => __( 'Content colour', 'text-domain' ),
                            'param_name' => 'vc_cta_clickable_box_colorscheme2',
                            'value' => __( '', 'text-domain' ),
                            'admin_label' => false,
                            'weight' => 0,
                        ),
                             
                    )
                )
            );
        
    } 
     
    // Element HTML
    public function vc_cta_clickable_box_html( $atts ) {

        extract(
            shortcode_atts(
                array(
                    'vc_cta_clickable_box_bg_image'        => '',
                    'vc_cta_clickable_box_image'        => '',
                    'vc_cta_clickable_box_heading'        => '',
                    'vc_cta_clickable_box_tooltip'        => '',
                    'vc_cta_clickable_box_link'        => '#',
                    'vc_cta_clickable_box_colorscheme1'        => '',
                    'vc_cta_clickable_box_colorscheme2'        => '',
                ), 
                $atts
            )
        );

        $href = vc_build_link($vc_cta_clickable_box_link);
        $bg_image_url = wp_get_attachment_image_src( $attachment_id = $vc_cta_clickable_box_bg_image, 'full' );
        $image_url = wp_get_attachment_image_src( $attachment_id = $vc_cta_clickable_box_image, 'box-icon' );
        $image_alt = get_post_meta( $image->id, '_wp_attachment_image_alt', true);

        $html = '
        <section>';
        if ($$href["url"]) {
            $html .= '<a href="'. $href["url"] .'" class="cta-box-a">';
        }
        $html .= '<div class="cta-box link" style="background-color:'. $vc_cta_clickable_box_colorscheme1 .';background-image:url('.$bg_image_url[0].');" title="'.$vc_cta_clickable_box_tooltip.'">
                <img src="'. $image_url[0] .'" alt="'. $image_alt .'" title="'. $vc_cta_clickable_box_heading .'" >
                <h3 style="color:'. $vc_cta_clickable_box_colorscheme2 .'!important;">'. $vc_cta_clickable_box_heading .'</h3>
            </div>';
            if ($href) {
               $html .= '</a>';
            }
        $html .= '</section>';

        return $html;

    } 
     
} // End Element Class
 
// Element Class Init
new cta_clickable_box();    
?>