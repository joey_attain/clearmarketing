<?php

// Element Class 
class cta_hover_text extends WPBakeryShortCode {
     
    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'vc_cta_hover_text_hover_mapping' ) );
        add_shortcode( 'vc_cta_hover_text_hover', array( $this, 'vc_cta_hover_text_hover_html' ) );
    }

    // Element Mapping
    public function vc_cta_hover_text_hover_mapping() {
         
        // Stop all if VC is not enabled
            if ( !defined( 'WPB_VC_VERSION' ) ) {
                    return;
            }
                 
            // Map the block with vc_map()
            vc_map( 
          
                array(
                    'name' => __('Grid Hover Text Box', 'text-domain'),
                    'base' => 'vc_cta_hover_text_hover',
                    'description' => __('Show CTA with an image and text on hover', 'text-domain'), 
                    'category' => __('ATTAIN Elements', 'text-domain'),
                    'params' => array(  

                        array(
                            'type' => 'attach_image',
                            'holder' => 'img',
                            'heading' => __( 'Image', 'text-domain' ),
                            'param_name' => 'vc_cta_hover_text_hover_image',
                            'value' => __( '', 'text-domain' ),
                            'admin_label' => false,
                            'weight' => 0,
                        ),

                        array(
                            'type' => 'textfield',
                            'holder' => 'h3',
                            'heading' => __( 'Heading', 'text-domain' ),
                            'param_name' => 'vc_cta_hover_text_hover_heading',
                            'value' => __( '', 'text-domain' ),
                            'admin_label' => false,
                            'weight' => 0,
                        ),

                        array(
                            'type' => 'textfield',
                            'heading' => __( 'Heading Size', 'text-domain' ),
                            'param_name' => 'vc_cta_hover_text_hover_heading_size',
                            'value' => __( '', 'text-domain' ),
                            'admin_label' => false,
                            'weight' => 0,
                        ),

                        array(
                            "type" => "textarea",
                            "holder" => "p",
                            "class" => "",
                            "heading" => __( "Content", "my-text-domain" ),
                            "param_name" => "vc_cta_hover_text_hover_content",
                            "description" => __( "Enter your content.", "my-text-domain" ),
                            'admin_label' => false,
                            'weight' => 0,
                         ),

                        array(
                            'type' => 'textfield',
                            'heading' => __( 'Content Size', 'text-domain' ),
                            'param_name' => 'vc_cta_hover_text_hover_content_size',
                            'value' => __( '', 'text-domain' ),
                            'admin_label' => false,
                            'weight' => 0,
                        ),

                        array(
                            'type' => 'colorpicker',
                            'holder' => '',
                            'heading' => __( 'Box colour', 'text-domain' ),
                            'param_name' => 'vc_cta_hover_text_hover_colorscheme1',
                            'value' => __( '', 'text-domain' ),
                            'admin_label' => false,
                            'weight' => 0,
                        )
                             
                    )
                )
            );
        
    } 
     
    // Element HTML
    public function vc_cta_hover_text_hover_html( $atts, $content, $tag ) {

        extract(
            shortcode_atts(
                array(
                    'vc_cta_hover_text_hover_image'        => '',
                    'vc_cta_hover_text_hover_heading'        => '',
                    'vc_cta_hover_text_hover_heading_size'        => '',
                    'vc_cta_hover_text_hover_colorscheme1'        => '',
                    'vc_cta_hover_text_hover_content'        => '',
                    'vc_cta_hover_text_hover_content_size'        => '',
                ), 
                $atts
            )
        );
        $atts['content'] = $content;

        $href = vc_build_link($vc_cta_hover_text_hover_link);
        $image_url = wp_get_attachment_image_src( $attachment_id = $vc_cta_hover_text_hover_image, 'box-icon' );
        $image_alt = get_post_meta( $image->id, '_wp_attachment_image_alt', true);

        $html = '
        <section>
        <div style="background-color:'. $vc_cta_hover_text_hover_colorscheme1 .';">
            <div class="cta-box cta-icon-text-hover" style="background-color:'. $vc_cta_hover_text_hover_colorscheme1 .';border-color:'. $vc_cta_hover_text_hover_colorscheme1 .';">

                <div class="off-hover">';
                    if($vc_cta_hover_text_hover_image) {
                        $html .= '<img src="'. $image_url[0] .'" alt="'. $image_alt .'">';
                    }
                    $html .= '<h3 style="font-size:'. $vc_cta_hover_text_hover_heading_size .';">'. $vc_cta_hover_text_hover_heading .'</h3>
                </div>';

               $html .= '<div class="on-hover" style="color:'. $vc_cta_hover_text_hover_colorscheme1 .'!important;">';
                    $html .= '<h3 style="font-size:'. $vc_cta_hover_text_hover_heading_size .';color:'. $vc_cta_hover_text_hover_colorscheme1 .'!important;">'. $vc_cta_hover_text_hover_heading .'</h3>
                              <p style="font-size:'. $vc_cta_hover_text_hover_content_size .';">'. $vc_cta_hover_text_hover_content .'</p>
                </div>
                
            </div>
            </div>
        </section>
        ';

        return $html;

    } 
     
} // End Element Class
 
// Element Class Init
new cta_hover_text();    
?>