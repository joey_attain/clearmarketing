<?php

// Element Class 
class cta_icon_text extends WPBakeryShortCode {
     
    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'vc_cta_icon_text_mapping' ) );
        add_shortcode( 'vc_cta_icon_text', array( $this, 'vc_cta_icon_text_html' ) );
    }

    // Element Mapping
    public function vc_cta_icon_text_mapping() {
         
        // Stop all if VC is not enabled
            if ( !defined( 'WPB_VC_VERSION' ) ) {
                    return;
            }
                 
            // Map the block with vc_map()
            vc_map( 
          
                array(
                    'name' => __('Grid Text Box', 'text-domain'),
                    'base' => 'vc_cta_icon_text',
                    'description' => __('Show text boxed CTA with an image', 'text-domain'), 
                    'category' => __('ATTAIN Elements', 'text-domain'),
                    'params' => array(  

                        array(
                            'type' => 'attach_image',
                            'holder' => 'img',
                            'heading' => __( 'Image', 'text-domain' ),
                            'param_name' => 'vc_cta_icon_text_image',
                            'value' => __( '', 'text-domain' ),
                            'admin_label' => false,
                            'weight' => 0,
                        ),

                        array(
                            'type' => 'textfield',
                            'holder' => 'div',
                            'heading' => __( 'Font Awesome Icon', 'text-domain' ),
                            'description' => __( 'E.g: fa-twitter', 'text-domain' ),
                            'param_name' => 'vc_cta_icon_text_icon',
                            'value' => __( '', 'text-domain' ),
                            'admin_label' => false,
                            'weight' => 0,
                        ),

                        array(
                            'type' => 'colorpicker',
                            'holder' => 'p',
                            'heading' => __( 'Icon colour', 'text-domain' ),
                            'param_name' => 'vc_cta_box_icon_color',
                            'value' => __( '', 'text-domain' ),
                            'admin_label' => false,
                            'weight' => 0,
                        ),

                        array(
                            "type" => "textarea_html",
                            "holder" => "div",
                            "class" => "",
                            "heading" => __( "Content", "my-text-domain" ),
                            "param_name" => "content",
                            "value" => __( "<p>I am test text block. Click edit button to change this text.</p>", "my-text-domain" ),
                            "description" => __( "Enter your content.", "my-text-domain" )
                         ),

                        array(
                            'type' => 'textarea',
                            'holder' => 'p',
                            'heading' => __( 'Tooltip content', 'text-domain' ),
                            'param_name' => 'vc_cta_clickable_box_tooltip',
                            'value' => __( '', 'text-domain' ),
                            'admin_label' => false,
                            'weight' => 0,
                        ),

                        array(
                            'type' => 'colorpicker',
                            'holder' => '',
                            'heading' => __( 'Box colour', 'text-domain' ),
                            'param_name' => 'vc_cta_icon_text_colorscheme1',
                            'value' => __( '', 'text-domain' ),
                            'admin_label' => false,
                            'weight' => 0,
                        ),

                        array(
                            'type' => 'colorpicker',
                            'holder' => '',
                            'heading' => __( 'Border colour', 'text-domain' ),
                            'param_name' => 'vc_cta_icon_text_border',
                            'value' => __( '', 'text-domain' ),
                            'admin_label' => false,
                            'weight' => 0,
                        )
                             
                    )
                )
            );
        
    } 
     
    // Element HTML
    public function vc_cta_icon_text_html( $atts, $content, $tag ) {

        extract(
            shortcode_atts(
                array(
                    'vc_cta_icon_text_image'        => '',
                    'vc_cta_icon_text_icon'        => '',
                    'vc_cta_clickable_box_tooltip'        => '',
                    'vc_cta_icon_text_colorscheme1'        => '',
                    'vc_cta_icon_text_border'        => '',
                    'vc_cta_box_icon_color' => ''
                ), 
                $atts
            )
        );
        $atts['content'] = $content;

        $href = vc_build_link($vc_cta_icon_text_link);
        $image_url = wp_get_attachment_image_src( $attachment_id = $vc_cta_icon_text_image, 'box-icon' );
        $image_alt = get_post_meta( $image->id, '_wp_attachment_image_alt', true);

        $html = '
        <section>
            <div class="cta-icon-text" style="background-color:'. $vc_cta_icon_text_colorscheme1 .';border-color:'. $vc_cta_icon_text_border .';" title="'.$vc_cta_clickable_box_tooltip.'"><div>';

                if($vc_cta_icon_text_image) {
                    $html .= '<img src="'. $image_url[0] .'" alt="'. $image_alt .'" title="'. $vc_cta_icon_text_heading .'">';
                }
                if($vc_cta_icon_text_icon) {
                    $html .= '<div><i class="fa '.$vc_cta_icon_text_icon.'" aria-hidden="true" style="color:'.$vc_cta_box_icon_color.';"></i></div>';
                }
                
                $html .= '<p>'. $atts['content'] .'</p>
            </div></div>
        </section>
        ';

        return $html;

    } 
     
} // End Element Class
 
// Element Class Init
new cta_icon_text();    
?>