<?php
 
// Element Class 
class cta_image extends WPBakeryShortCode {
     
    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'vc_cta_image_mapping' ) );
        add_shortcode( 'vc_cta_image', array( $this, 'vc_cta_image_html' ) );
    }

    // Element Mapping
    public function vc_cta_image_mapping() {
         
        // Stop all if VC is not enabled
            if ( !defined( 'WPB_VC_VERSION' ) ) {
                    return;
            }
                 
            // Map the block with vc_map()
            vc_map( 
          
                array(
                    'name' => __('Grid Image', 'text-domain'),
                    'base' => 'vc_cta_image',
                    'description' => __('Image with popup', 'text-domain'), 
                    'category' => __('ATTAIN Elements', 'text-domain'),
                    'params' => array(  

                        array(
                            'type' => 'attach_image',
                            'holder' => 'img',
                            'heading' => __( 'Image', 'text-domain' ),
                            'param_name' => 'vc_cta_image',
                            'value' => __( '', 'text-domain' ),
                            'admin_label' => false,
                            'weight' => 0,
                        ),

                        array(
                            'type' => 'attach_image',
                            'holder' => 'img',
                            'heading' => __( 'Preview Thumbnail Image', 'text-domain' ),
                            'description' => __( 'The preview image displayed in the grid before the popup', 'text-domain' ),
                            'param_name' => 'vc_cta_preview_image',
                            'value' => __( '', 'text-domain' ),
                            'admin_label' => false,
                            'weight' => 0,
                        ),

                        array(
                            'type' => 'textfield',
                            'holder' => '',
                            'heading' => __( 'Mobile Height', 'text-domain' ),
                            'description' => __( 'Height of container on mobiles. E.G. 300px. Leaving this blank will disable on mobile', 'text-domain' ),
                            'param_name' => 'vc_cta_image_mobile_height',
                            'value' => __( '', 'text-domain' ),
                            'admin_label' => false,
                            'weight' => 0,
                        ),

                        array(
                            "type" => "textarea",
                            "holder" => "p",
                            "class" => "",
                            "heading" => __( "Top Hover Text", "my-text-domain" ),
                            "param_name" => "vc_cta_hover_text_hover_top_content",
                            "description" => __( "Enter top content to show on hover.", "my-text-domain" ),
                            'admin_label' => false,
                            'weight' => 0,
                         ),

                        array(
                            'type' => 'textfield',
                            'heading' => __( 'Hover Top Text Size', 'text-domain' ),
                            'param_name' => 'vc_cta_hover_text_hover_top_content_size',
                            'value' => __( '', 'text-domain' ),
                            'admin_label' => false,
                            'weight' => 0,
                        ),

                        array(
                            "type" => "textarea",
                            "holder" => "p",
                            "class" => "",
                            "heading" => __( "Hover Text", "my-text-domain" ),
                            "param_name" => "vc_cta_hover_text_hover_content",
                            "description" => __( "Enter content to show on hover.", "my-text-domain" ),
                            'admin_label' => false,
                            'weight' => 0,
                         ),

                        array(
                            'type' => 'textfield',
                            'heading' => __( 'Hover Text Size', 'text-domain' ),
                            'param_name' => 'vc_cta_hover_text_hover_content_size',
                            'value' => __( '', 'text-domain' ),
                            'admin_label' => false,
                            'weight' => 0,
                        ),

                        array(
                            'type' => 'colorpicker',
                            'holder' => '',
                            'heading' => __( 'Hover Text Color', 'text-domain' ),
                            'param_name' => 'vc_cta_hover_text_hover_colorscheme1',
                            'value' => __( '', 'text-domain' ),
                            'admin_label' => false,
                            'weight' => 0,
                        ),

                        array(
                            'type' => 'colorpicker',
                            'holder' => '',
                            'heading' => __( 'Hover background Color', 'text-domain' ),
                            'param_name' => 'vc_cta_hover_text_hover_bg_color',
                            'value' => __( '', 'text-domain' ),
                            'admin_label' => false,
                            'weight' => 0,
                        ),

                        array(
                            'type' => 'textfield',
                            'holder' => '',
                            'heading' => __( 'Youtube Video Link', 'text-domain' ),
                            'description' => __( 'If you want to open a video instead of an image, enter its link', 'text-domain' ),
                            'param_name' => 'vc_cta_image_video_link',
                            'value' => __( '', 'text-domain' ),
                            'admin_label' => false,
                            'weight' => 0,
                        ),

                        array(
                            'type' => 'checkbox',
                            'heading' => __( 'Open Custom Link', 'text-domain' ),
                            'param_name' => 'vc_cta_image_custom_link',
                            'value' => __( '', 'text-domain' ),
                            'admin_label' => false,
                            'weight' => 0,
                        ),

                        array(
                            'type' => 'vc_link',
                            'holder' => 'a',
                            'heading' => __( 'Link', 'text-domain' ),
                            'param_name' => 'vc_cta_image_link',
                            'value' => __( '', 'text-domain' ),
                            'admin_label' => false,
                            'weight' => 0,
                        ),

                        array(
                            'type' => 'dropdown',
                            'heading' => __( 'Type',  "text-domain" ),
                            'param_name' => 'vc_cta_image_type',
                            'value' => array(
                              __( 'Default',  "my-text-domain"  ) => '',
                              __( 'Non-Clickable',  "my-text-domain"  ) => 'noClick',
                            ),
                            "description" => __( "Changes the function of the element", "text-domain" )
                        ),
                             
                    )
                )
            );                             
        
    } 
     
    // Element HTML
    public function vc_cta_image_html( $atts ) {

        extract(
            shortcode_atts(
                array(
                    'vc_cta_image'        => '',
                    'vc_cta_preview_image'        => '',
                    'vc_cta_image_mobile_height'        => '0',
                    'vc_cta_image_video_link'        => '',
                    'vc_cta_image_custom_link'        => '',
                    'vc_cta_image_link'        => '',
                    'vc_cta_image_type'  => '',
                    'vc_cta_hover_text_hover_top_content'        => '',
                    'vc_cta_hover_text_hover_top_content_size'        => '',
                    'vc_cta_hover_text_hover_content'        => '',
                    'vc_cta_hover_text_hover_content_size'        => '',
                    'vc_cta_hover_text_hover_colorscheme1'        => '',
                    'vc_cta_hover_text_hover_bg_color'  => '',
                ), 
                $atts
            )
        );

        $isMobile = $atts['vc_cta_image_mobile'];

        $href = vc_build_link($vc_cta_image_link);
        $image_url_preview = wp_get_attachment_image_src( $attachment_id = $vc_cta_preview_image, 'full' );
        $image_url_full = wp_get_attachment_image_src( $attachment_id = $vc_cta_image, 'full' );

        $attachment = get_post($attachment_id);
        $alt = get_post_meta($attachment_id, '_wp_attachment_image_alt', true);
        $caption = $attachment->post_excerpt;

        if($vc_cta_image_video_link != '') {
            $id = uniqid();
            $class = "cta-video";
            $href = '#'.$id;
        } else if($vc_cta_image_custom_link) {
            $class = "cta-image";
            $href = $href['url'];
        } else {
            $class ="cta-image hasPopup";
            $href = $image_url_full[0];
        }

        if($vc_cta_hover_text_hover_content != '') {
            $hoverClass = 'hover-active';
        } else {
            $hoverClass = '';
        }

        if($vc_cta_image_type == 'noClick') {
            $anchor = '';
        } else {
            $anchor = '<a href="'.$href.'" title="'.$caption.'"></a>';
        }

        $html = '
        <section>';
            if($image_url_preview != '') {
                $html .= ' <div class="'. $class .' '.$hoverClass.'" style="background-image: url('.$image_url_preview[0].');min-height:'.$vc_cta_image_mobile_height.';">'.$anchor;
                
                if($vc_cta_hover_text_hover_content != '') {
                $html .= '<div class="on-hover" style="color:'. $vc_cta_hover_text_hover_colorscheme1 .'!important;background-color:'. $vc_cta_hover_text_hover_bg_color .';">';
                        $html .= '<p class="hover-top-text" style="font-size:'. $vc_cta_hover_text_hover_top_content_size .';color:'. $vc_cta_hover_text_hover_colorscheme1 .'!important;">'. $vc_cta_hover_text_hover_top_content .'</p>';
                        $html .= '<p style="font-size:'. $vc_cta_hover_text_hover_content_size .';color:'. $vc_cta_hover_text_hover_colorscheme1 .'!important;">'. $vc_cta_hover_text_hover_content .'</p>
                    </div>';
                }
                
                $html .= '</div>';
            } else {
                $html .= ' <div class="'. $class .'" style="background-image: url('.$image_url_full[0].');min-height:'.$vc_cta_image_mobile_height.';">'.$anchor;
            }

            

        $html .= '</section>';

        if($vc_cta_image_video_link != '') {
            $html .= '
                <div class="post-popup white-popup mfp-hide" id="'.$id.'">
                
                <div class="video-responsive">
                    <iframe width="800" height="auto" src="'.$vc_cta_image_video_link.'" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                </div>
                </div>
            ';
        }

        return $html;

    } 
     
} // End Element Class
 
// Element Class Init
new cta_image();    
?>