<?php
 
// Element Class 
class cta_strip extends WPBakeryShortCode {
     
    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'vc_cta_strip_mapping' ) );
        add_shortcode( 'vc_cta_strip', array( $this, 'vc_cta_strip_html' ) );
    }

    // Element Mapping
    public function vc_cta_strip_mapping() {
         
        // Stop all if VC is not enabled
            if ( !defined( 'WPB_VC_VERSION' ) ) {
                    return;
            }
                 
            // Map the block with vc_map()
            vc_map( 
          
                array(
                    'name' => __('Grid Strip', 'text-domain'),
                    'base' => 'vc_cta_strip',
                    'description' => __('Small strip to sit over backround image', 'text-domain'), 
                    'category' => __('ATTAIN Elements', 'text-domain'),
                    'params' => array(  

                        array(
                            'type' => 'textfield',
                            'holder' => 'p',
                            'heading' => __( 'Top line', 'text-domain' ),
                            'param_name' => 'vc_cta_strip_top_line',
                            'value' => __( '', 'text-domain' ),
                            'admin_label' => false,
                            'weight' => 0,
                        ),

                        array(
                            'type' => 'textfield',
                            'holder' => 'p',
                            'heading' => __( 'Bottom line', 'text-domain' ),
                            'param_name' => 'vc_cta_strip_bottom_line',
                            'value' => __( '', 'text-domain' ),
                            'admin_label' => false,
                            'weight' => 0,
                        ),

                        array(
                            'type' => 'vc_link',
                            'holder' => 'a',
                            'heading' => __( 'Link', 'text-domain' ),
                            'param_name' => 'vc_cta_strip_link',
                            'value' => __( '', 'text-domain' ),
                            'admin_label' => false,
                            'weight' => 0,
                        ),

                        array(
                            'type' => 'textfield',
                            'holder' => 'p',
                            'heading' => __( 'Link Text', 'text-domain' ),
                            'param_name' => 'vc_cta_strip_link_text',
                            'value' => __( '', 'text-domain' ),
                            'admin_label' => false,
                            'weight' => 0,
                        ),
                             
                    )
                )
            );                             
        
    } 
     
    // Element HTML
    public function vc_cta_strip_html( $atts ) {

        extract(
            shortcode_atts(
                array(
                    'vc_cta_strip_top_line'        => '',
                    'vc_cta_strip_bottom_line'        => '',
                    'vc_cta_strip_link'        => 'Link',
                    'vc_cta_strip_link_text'        => 'View More',
                ), 
                $atts
            )
        );

        $href = vc_build_link($vc_cta_strip_link);

        $html = '
        <section>
            <div class="cta-strip">
                <div class="cta-strip-left">
                    <h3>'.esc_html($vc_cta_strip_top_line).'</h3>
                    <p>'.esc_html($vc_cta_strip_bottom_line).'</p>
                </div>
                <div class="cta-strip-right">
                    <a href="'.$href["url"].'" class="btn-hollow-white-alt">'.esc_html($vc_cta_strip_link_text).'</a>
                </div>
            </div>
        </section>
        ';

        return $html;

    } 
     
} // End Element Class
 
// Element Class Init
new cta_strip();    
?>